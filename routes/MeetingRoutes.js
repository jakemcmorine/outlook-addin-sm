const express = require('express')
const meetingRouter = express.Router()
const meetinglists = require('../controllers/MeetingController')

meetingRouter.post('/count', meetinglists.meetingCountOfUser);

module.exports = meetingRouter;
