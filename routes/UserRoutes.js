const express = require('express');
const userRouter = express.Router();
const userLists = require('../controllers/UserController')
const meetinglists = require('../controllers/MeetingController')
const { EnsureAuth } = require('../modules/EnsureAuth');

userRouter.get('/:emailId', userLists.userDetailsByEmailId);
userRouter.get('/footer/:email', meetinglists.getFooter);
userRouter.post('/getToken', EnsureAuth, userLists.getToken);
userRouter.post('/logout', EnsureAuth, userLists.logout);

module.exports = userRouter;
