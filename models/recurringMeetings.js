'use strict'
module.exports = (sequelize, DataTypes) => {

  const recurring_meetings = sequelize.define('recurring_meetings', {
    calendar_type: DataTypes.STRING,
    recurring_meeting_id : { type : DataTypes.INTEGER,  primaryKey : true , autoIncrement: true },
    start_date : DataTypes.DATE,         
    end_date : DataTypes.DATE,
    is_all_day : DataTypes.STRING,
    start_day : DataTypes.STRING,
    repeat_every : DataTypes.INTEGER,
    frequency_type : DataTypes.STRING,
    weekdays_selected : DataTypes.STRING,
    day_index :  DataTypes.STRING,
    subject : DataTypes.STRING,      
    location : DataTypes.STRING,           
    type : DataTypes.STRING,               
    TimeZoneOffset : DataTypes.STRING,     
    TimeZoneName : DataTypes.STRING,       
    cancelled_at :DataTypes.DATE,
    created_at : DataTypes.DATE,
    updated_at : DataTypes.DATE,
    user_id : DataTypes.STRING,          
    outlook_meeting_id : DataTypes.STRING,
    organizer : DataTypes.STRING,
    org_id : DataTypes.INTEGER,         
    // initiativeId : DataTypes.INTEGER,       
    countable : DataTypes.INTEGER,
    next_meeting_date : DataTypes.DATE,
    resource : DataTypes.STRING,
    resource_id : DataTypes.STRING,
    subscription_id : DataTypes.STRING

  }, {
    timestamps: false
})

recurring_meetings.associate = function (models) {
    // associations can be defined here
    // recurring_meetings.hasMany(models.recurring_meeting_invitees, {as: 'invitees'} , { foreignKey: 'recurring_meeting_id' })
  }
  return recurring_meetings
}

