'use strict';
module.exports = (sequelize, DataTypes) => {
    const plan_details = sequelize.define('user_plans', {
        plan_id: DataTypes.INTEGER,
        org_id: DataTypes.INTEGER,
    }, {});
    plan_details.associate = function (models) {
        // associations can be defined here
    };
    plan_details.planDetailsByOrg = async function (org_id) {
        const list = await sequelize.query(`SELECT P.feature_id FROM user_plans AS UP
            LEFT JOIN plans AS P ON P.plan_details_id = UP.plan_id
            WHERE org_id = ${org_id}`,
            { type: sequelize.QueryTypes.SELECT }).then(function (result) {
                //console.log("Here \n",result);
                return result;
            });
        return list;
    };
    plan_details.findOnePlan = async function (org_id) {
        const list = await sequelize.query(`SELECT * FROM user_plans WHERE org_id = ${org_id}`,
            { type: sequelize.QueryTypes.SELECT }).then(function (result) {
                //console.log("Here \n",result);
                return result;
            });
        return list;
    };
    return plan_details;
};

