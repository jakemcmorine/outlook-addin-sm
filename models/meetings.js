'use strict'
module.exports = (sequelize, DataTypes) => {
  const meetings = sequelize.define('meetings', {
    outlook_meeting_id: DataTypes.STRING,
    meeting_id: DataTypes.STRING,
    subject: DataTypes.STRING,
    location: DataTypes.STRING,
    type: DataTypes.STRING,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    TimeZoneOffset: DataTypes.STRING,
    TimeZoneName: DataTypes.STRING,
    cancellation_date: DataTypes.DATE,
    user_id: DataTypes.NUMBER,
    initiativeId: DataTypes.NUMBER,
    org_id: DataTypes.NUMBER,
    arnName: DataTypes.STRING,
    countable: DataTypes.NUMBER,
    rm_id : DataTypes.STRING,
    all_Day : DataTypes.STRING
  }, {})
  meetings.associate = function (models) {
    // associations can be defined here
    meetings.hasMany(models.meeting_invites, { foreignKey: 'meeting_id' })
  }
  
  meetings.getLatestRmInstances = async function (rmId, startDate) {
    const rmInstances = await sequelize.query(`select * from meetings where start_date >= "${startDate}" and rm_id = "${rmId}" and cancellation_date is null`,
    { type: sequelize.QueryTypes.SELECT }).then(function (result) {
      // console.log("Here \n",result);
      return result
    })
    return rmInstances;
  }

  meetings.getMeetingDetails = async function (meetingId, checkMeeting) {
    var list = [];
    let checkMeetDone = [];
    if (checkMeeting){
      checkMeetDone = await sequelize.query(`SELECT M.id as MeetingId,M.meeting_id as web_meeting_id,M.*,MI.* FROM meetings AS M LEFT JOIN meeting_invites AS MI ON M.id = MI.meeting_id WHERE M.outlook_meeting_id = "${meetingId}" and MI.emailsent_date IS NOT NULL`,
      { type: sequelize.QueryTypes.SELECT }).then(function (result) {
        // console.log("Here \n",result);
        return result
      })
    }
    if (checkMeetDone.length > 0) {
      console.log("normal - meeting certificate already sent!... not able to update meeting - normal meeting flow");
    } else {
      list = await sequelize.query(`SELECT M.id as MeetingId,M.meeting_id as web_meeting_id,M.*,MI.* FROM meetings AS M LEFT JOIN meeting_invites AS MI ON M.id = MI.meeting_id WHERE M.outlook_meeting_id = "${meetingId}"`,
        { type: sequelize.QueryTypes.SELECT }).then(function (result) {
          // console.log("Here \n",result);
          return result
        })
    }
    return list
  }

  /**
   * Following method checks if for today's meeting certificate is triggerred or not.
   */
  meetings.checkIfCertTrigForMeeting = async (meetingId) => {
    try {
      console.log(`-----Checking Cert Trigger status for today's meeting with meeting id : ${meetingId}`);
      let query = `SELECT emailsent_date FROM meeting_invites LEFT JOIN  meetings on meetings.id = meeting_invites.meeting_id where 1= 1 and meetings.meeting_id = ${meetingId}`;
      
      let emailSentDatesForExistingMeeting = await sequelize.query(query);
      console.log(emailSentDatesForExistingMeeting);
      let isSent = false;
      if(emailSentDatesForExistingMeeting !== null && emailSentDatesForExistingMeeting.length !==0) { 
          for(let i=0;i< emailSentDatesForExistingMeeting.length ; i ++) {
              isSent = emailSentDatesForExistingMeeting[i].emailsent_date !== null;
              if(isSent) {
                  break;
              }
          }
          return isSent
      }
      else return isSent;
    } catch(error) {
        console.log("Error Occurred while getting dates for existing meeting ---", error);
      return false;
    }
  }


  meetings.getCancelMeetingDetailsWithNullCheck = async function (meetingId) {
    let checkMeetDone = await sequelize.query(`SELECT M.id as MeetingId,M.meeting_id as web_meeting_id,M.*,MI.* FROM meetings AS M LEFT JOIN meeting_invites AS MI ON M.id = MI.meeting_id WHERE M.meeting_id = "${meetingId}" and MI.emailsent_date IS NOT NULL`,
        { type: sequelize.QueryTypes.SELECT }).then(function (result) {
          // console.log("Here \n",result);
          return result
        })
    return checkMeetDone;
  }

  return meetings
}
