'use strict';
module.exports = (sequelize, DataTypes) => {
    const time_zone_details = sequelize.define('time_zone_details', {
        id : { type : DataTypes.INTEGER , primaryKey : true},
        time_zone_abbr : DataTypes.STRING,
        time_zone_name : DataTypes.STRING,
        time_zone_offset :DataTypes.STRING, 

    },{
      timestamps: false
  });
    time_zone_details.associate = function(models) {
    // associations can be defined here
  };
  return time_zone_details;
};