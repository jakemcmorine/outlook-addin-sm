'use strict';
module.exports = (sequelize, DataTypes) => {
    const setting = sequelize.define('user_token', {
        email: {
            type : DataTypes.STRING,
            primaryKey: true,
        },
        accessToken: DataTypes.STRING,
        refreshToken: DataTypes.STRING,
        webhookId: DataTypes.STRING,
        subscriptionTime: DataTypes.STRING,
    }, {
        timestamps: false
    });
  setting.associate = function(models) {
    // associations can be defined here
  };
  return setting;
};