'use strict'

const fs = require('fs');
const path = require('path');
const {Sequelize}=require('sequelize');
const basename = path.basename(__filename)
// const env = process.env.NODE_ENV || 'local'
// const config = require(__dirname + '/../config/config.json')[env]
const db = {}

let config = {
  "username": process.env.DB_USER,
  "password": process.env.DB_PASSWORD,
  "database": process.env.DB_NAME,
  "host": process.env.DB_HOST,
  "dialect": "mysql",
  "operatorsAliases": false
};


let sequelize

sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, config, {
  logging: false
});

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(file => {
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
});


db.sequelize = sequelize

module.exports = db
