'use strict'

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: {
      unique: true,
      type: DataTypes.STRING
    },
    password: DataTypes.STRING,
    status: DataTypes.STRING,
    type: DataTypes.STRING,
    orginasation_id: DataTypes.INTEGER(12),
    webUrl: DataTypes.STRING,
    phone: DataTypes.STRING,
    emailEnabled: DataTypes.BOOLEAN,
    isMailVerified: DataTypes.BOOLEAN,
    currency: DataTypes.STRING(3),
    tax: DataTypes.FLOAT,
    initiativeId: DataTypes.INTEGER,
    techSupportEmail: DataTypes.STRING,
    companyName: DataTypes.STRING,
    companyLogo: DataTypes.TEXT,
    freeTrial: DataTypes.BOOLEAN,
    trialEnd: DataTypes.DATE,
    treeNumber: DataTypes.INTEGER,
    costPerTree: DataTypes.FLOAT,
    image_1: DataTypes.TEXT,
    image_2: DataTypes.TEXT,
    image_3: DataTypes.TEXT,
    image_4: DataTypes.TEXT
  }, {});

  user.getUserDetails = async function (email) {
    
    // const list = await sequelize.query(`SELECT users.*, u1.status as user_status, u1.trialEnd,  FROM users LEFT JOIN users u1 on users.orginasation_id = u1.id where LOWER(users.email) = '${email.toLocaleLowerCase()}'`,
    const list = await sequelize.query(`SELECT users.*, u1.status as user_status, u1.trialEnd, u1.isMailVerified as mailVerified, u1.isActive as IPActive FROM users LEFT JOIN users u1 on users.orginasation_id = u1.id where LOWER(users.email) ='${email.toLocaleLowerCase()}'`,
      { type: sequelize.QueryTypes.SELECT }).then(function (result) {
        // console.log("Here \n",result);
        return result
      })
    return list
  }

  user.getOrgSettings = async function (userId) {
    const list = await sequelize.query(`SELECT  users.id,u1.status as user_status,u1.trialEnd, settings.id as setting_id,users.orginasation_id,settings.emailSend,
          settings.hour,settings.minute,settings.exclude_meeting,settings.domain_url
          FROM    users 
          LEFT JOIN settings ON settings.user_id = users.orginasation_id 
          LEFT JOIN users u1 on users.orginasation_id=u1.id
          WHERE users.id = '${userId}'`,
      { type: sequelize.QueryTypes.SELECT }).then(function (result) {
        // console.log("Here \n",result);
        return result
      })
    return list
  }

  return user
}
