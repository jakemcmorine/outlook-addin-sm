'use strict';
module.exports = (sequelize, DataTypes) => {
  const setting = sequelize.define('setting', {
    user_id: DataTypes.INTEGER,
    emailSend: DataTypes.STRING,
    hour: DataTypes.INTEGER,
    minute: DataTypes.INTEGER,
    exclude_meeting: DataTypes.STRING,
    info_url: DataTypes.STRING,
    domain_url: DataTypes.STRING,
    footer: DataTypes.STRING,
    meetingPlatform: DataTypes.STRING,
    budgetStatus: DataTypes.INTEGER,
    budget: DataTypes.INTEGER,
  }, {});
  setting.associate = function(models) {
    // associations can be defined here
  };
  return setting;
};