'use strict'
module.exports = (sequelize, DataTypes) => {
  const tree_count = sequelize.define('tree_count', {
    user_id: DataTypes.INTEGER,
    org_id: DataTypes.INTEGER,
    trees: DataTypes.INTEGER,
    date: DataTypes.DATE,
    day: DataTypes.INTEGER,
    month: DataTypes.INTEGER,
    year: DataTypes.INTEGER,
    
  }, {})
  tree_count.associate = function (models) {
  }

  tree_count.getTreeCountWithSubscription = async function (orginasation_id) {
    const rmInstances = await sequelize.query(`SELECT SUM(tree_counts.trees) AS trees FROM tree_counts
      LEFT JOIN subscriptions ON subscriptions.subscriptionId=tree_counts.subscriptionId
      WHERE tree_counts.subscriptionId IS NOT NULL AND subscriptions.status = "active" AND
      org_id = ${orginasation_id} AND tree_counts.date >= subscriptions.cycleStartDate AND tree_counts.date <= subscriptions.billingDate`,
      { type: sequelize.QueryTypes.SELECT }).then(function (result) {
        // console.log("Here \n",result);
        return result
      })
    return rmInstances;
  }
  return tree_count
}
