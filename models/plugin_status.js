'use strict';
module.exports = (sequelize, DataTypes) => {
    const plugin_status = sequelize.define('plugin_status', {
        email: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        plugin_type: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        last_access: DataTypes.DATE,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
        disabled: DataTypes.INTEGER,
    }, {
        timestamps: false
    });
    plugin_status.associate = function (models) {
        // associations can be defined here
    };

    plugin_status.changePluginStatus = async function (status, email, pluginType) {
        const rmInstances = await sequelize.query(`UPDATE plugin_status SET disabled = ${status} WHERE plugin_status.email = '${email}' AND plugin_status.plugin_type = '${pluginType}'`,
            { type: sequelize.QueryTypes.UPDATE }).then(function (result) {
                // console.log("Here \n",result);
                return result
            })
        return rmInstances;
    }
    return plugin_status;
};