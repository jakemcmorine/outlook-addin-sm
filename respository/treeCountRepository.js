const Sequelize = require('sequelize')
const Op = Sequelize.Op

const treeCountModel = require('../models').tree_count

class treeCountRepository {

    async getTreeCount (userId, isUser) {
        console.log("user Id", userId);
        let type;
        if(isUser) {
          type = "user_id";
        } else {
          type = "org_id";
        }
        const treeCount = await treeCountModel.findAll({
            attributes: 
            [[Sequelize.fn('sum', Sequelize.col('trees')), 'treeCount']],
            where: {
                [type]: userId
              },
            raw: true
        })
      if (treeCount && treeCount[0].treeCount == null)
        treeCount[0].treeCount = 0
      return treeCount;
      }
}

module.exports = new treeCountRepository()