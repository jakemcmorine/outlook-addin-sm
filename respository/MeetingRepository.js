const sequelize = require('sequelize')
const db =  require('../models/index');
const Sequelize=require('sequelize');
const Op = Sequelize.Op;
const userModel = require('../models').user;
const settingModel = require('../models').setting;
const intiativeModel = require('../models').intiative;
const meetingModel = require('../models').meetings;
const meetingInviteeModel = require('../models').meeting_invites;
const meeting_invites_Model = require('../models').meeting_invites;
const userPlansModel = require("../models").user_plans;
const treeCountModel = require("../models").tree_count;

class MeetingRepository {

    async meetingCount (query) {
        const meetingCount = await meetingModel.count({where: query})
        // console.log("meetingCount", meetingCount);
        return meetingCount
      }
    
    async meetingInviteCount (where) {
        const meetingCount = await meetingModel.findAll({
          where: where,
          //   attributes: {
          //       include: [[Sequelize.fn("COUNT", Sequelize.col("meeting_invites.meeting_id")), "historyModelCount"]]
          //   },
          attributes: [
            // [Sequelize.fn('count', Sequelize.col('meetings.id')), 'total_user'],
            [Sequelize.fn('count', Sequelize.col('meeting_invites.meeting_id')), 'meeting_invites_Count']
          ],
          include: [{
            model: meeting_invites_Model,
            where: {
              emailsent_date: {
                [Op.ne]: null
              }
            }
          }]
    
        })
        // console.log(meetingCount)
        return meetingCount
    }

    meetingCountOfUser(userId) {
        
        return new Promise((resolve , reject ) => {
            db.sequelize.model('meetings').findAll({
                where : {
                    user_id : userId
                },
                attributes : [
                    'countable',
                    [sequelize.fn('COUNT', sequelize.col('countable')), 'count'],
                ],
                group: 'countable',
                raw: true,  // Putting this field avoids giving extra information

            }).then(result => {
                console.log("Successfully Made query to Meetings table");
                resolve(result);
            }).catch(error => {
                console.log("Error Occurred while getting details from meetin");
                reject(error);
            });
        });


    }

    meetingCountOfOrg(userId) {
        return new Promise((resolve , reject ) => {
            db.sequelize.model('meetings').findAll({
                where : {
                    org_id : userId
                },
                attributes : [
                    'countable',
                    [sequelize.fn('COUNT', sequelize.col('countable')), 'count'],
                ],
                group: 'countable',
                raw: true,  // Putting this field avoids giving extra information

            }).then(result => {
                console.log("Successfully Made query to Meetings table");
                resolve(result);
            }).catch(error => {
                console.log("Error Occurred while getting details from meetin");
                reject(error);
            });
        });
    }

    async getFooter(email) {
        console.log(`getFooter email : ${email}`);
        let userDetails = await userModel.getUserDetails( email);
        let budgetExceeded = false,footer=null;
        if (userDetails.length){
            let userSettings = await settingModel.findOne({ where :{user_id : userDetails[0].orginasation_id}});
            let planDetails = await userPlansModel.planDetailsByOrg( userDetails[0].orginasation_id);
            // console.log(userSettings);   
            let budgetAccess = planDetails.some(ft => ft.feature_id == 4);
            let footerAccess = planDetails.some(ft => ft.feature_id == 1);
            if (budgetAccess && userSettings.budgetStatus == 1) {
                let resp4 = await treeCountModel.getTreeCountWithSubscription(userDetails[0].orginasation_id);
                console.log(resp4);
                if (resp4[0].trees >= userSettings.budget) {
                    budgetExceeded = true;
                }
            }
            
            if (userSettings){
                footer = userSettings.footer;
            }
            if (footerAccess == false || !userSettings || userSettings.footer == null || userSettings.footer == undefined || userSettings.footer == '') {
                let initiativeData = await intiativeModel.findOne({ where: { id : userDetails[0].initiativeId}});
                footer = initiativeData.mail5_body;
            }
    
            const firstDate = new Date().setHours(12, 0, 0, 0);
            const secondDate = new Date(userDetails[0].trialEnd).getTime();
            let user_status = userDetails[0].user_status
            if (secondDate > firstDate) {
                user_status = 'active'
            }
            
            // if (user_status != 'active') {
            //     footer = ''
            // }
            if (user_status != 'active' || userDetails[0].mailVerified != 1 || userDetails[0].IPActive == 0 || budgetExceeded == true) {
                footer = ''
            }
        }
        return footer
    }

    async checkUserIp(userId) {
        let isUserIp = await  db.sequelize.model('meetings').findOne({
            where : {
                org_id : userId
            }
        })
        if(isUserIp !== null) {
            // console.log("---------------- Is User IP : -----------" , isUserIp['dataValues']);    
            return true;
        }
        else return false;
    }

    async meetingData(where) {
        
        let meeting = await meetingModel.findOne(where);
        return meeting
    }

    async removeMeeting(where) {
        let meeting = await meetingModel.destroy(where);
        return meeting
    }

    async removeInvitees(id) {
        let meeting = await meetingInviteeModel.destroy({ where: { meeting_id : id}});
        return meeting
    }

    async insertInvitees(data) {
        let meeting = await meetingInviteeModel.bulkCreate(data);
        return meeting
    }

    async updateMeeting(data, id) {
        try {
            let updateMeeting = await meetingModel.update(data, {
                where: { id: id },
            });
            return updateMeeting;
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    async createMeeting(data, invitees) {
        try {
            const meeting = await meetingModel.create(data);
            return meeting;
            
            // TODO: save meeting invites
            // const invitess = await meetingInviteeModel.bulkCreate({

            // })
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    async getMeetingDetails(meetingId, checkMeeting= false) {
        let meeting = await meetingModel.getMeetingDetails(meetingId, checkMeeting);
        return meeting
    }

    async getExistingInstances(rmId, startDate) {
        let rmInstances = await meetingModel.getLatestRmInstances(rmId, startDate);
        console.log("-----------Exisiting RM instances --------------", rmInstances);
        return rmInstances;
    }

    async cancelRmInstances(rmId) {
        // cancellation_date : new Date().toISOString(),
        // console.log("-----------Removing RM Instances ---------------");
        /**
         * Check if it is an ALL DAY event . 
         * IF YES : keep Date with hours : 00:00:00
         * IF NOT : Keep Date with new Date().toISOString();
         */
        let cancelledRmInstances = await meetingModel.findAll({
            where : {
                [Op.and] : [
                    {rm_id : rmId},
                    {
                        [Op.or] : [
                            {
                                [Op.and] : [
                                    { start_date : { [Op.gte] : new Date().toISOString()} },
                                    { all_Day : 'N'}
                                ]
                            },
                            {
                                [Op.and] : [
                                    { start_date : { [Op.gte] : new Date(new Date().setUTCHours(0,0,0,0)).toISOString()} },
                                    { all_Day : 'Y' }
                                ]
                            }
                        ]
                    }
                ]
            }
            // where : { rm_id : rmId, start_date : {
            //     [Op.gt] : new Date().toISOString()
            // } }
        });
        
        let meetingIds = [];
        let stateMachineArn = [];
        if(cancelledRmInstances !== null || cancelledRmInstances['dataValues'] !== undefined) {
            meetingIds = cancelledRmInstances.map(x => x['dataValues']['id']);
            stateMachineArn = cancelledRmInstances.map(x => x['dataValues']['arnName']);
            console.log("arnNames are : ", stateMachineArn);
            // return meetingIds
        }
        if(meetingIds !== null && meetingIds.length !== 0) {
            await meetingInviteeModel.destroy({ where : {
                meeting_id : {
                    [Op.in] : meetingIds
                }
            }})
            console.log("------------Meetings ids to be deleted : ----------", meetingIds)
            await meetingModel.destroy({ 
                where : {
                    id : {
                        [Op.in] : meetingIds
                    }
                }
            });

            return stateMachineArn;
        }

       
    }

    async checkIfCertTrigForMeeting(meetingId) {
        // return await meetingModel.checkIfCertTrigForMeeting(meetingId);
        let result =  await meetingModel.getCancelMeetingDetailsWithNullCheck(meetingId);
        return result.length > 0 ? true : false;
    }

    async getMeetingByIcalUid(iCalUid) {
        try {
            let startDate = new Date().setUTCHours(0,0,0);
            let endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);
            endDate = endDate.setUTCHours(0,0,0);

            console.log(`-----Getting today's meeting for iCalUid : ${iCalUid} with start : ${startDate} and end : ${endDate}`);

            let meetingDetails = await meetingModel.findAll({
                where : {
                    [Op.and] : [
                        {outlook_meeting_id : iCalUid},
                        { start_date : { [Op.gte] : new Date(startDate).toISOString() }},
                        { end_date : { [Op.lte] : new Date(endDate).toISOString() } }
                    ]
                }
            });
            console.log(meetingDetails);
            // let meetingIdQuery = `select id, meeting_id from meetings where 
            // outlook_meeting_id='${iCalUid}' and start_date >= '${startDate} 00:00:00' and start_date <= '${endDate} 00:00:00'`;

            // let meetingId = await pool.query(meetingIdQuery);
            if(meetingDetails.length) {
                console.log("Meeting id is : " , meetingDetails[0].id);

                let isCertTrig = await meetingInviteeModel.findAll({
                    where : {
                        [Op.and] : [
                            {meeting_id : meetingDetails[0].id},
                            {emailsent_date : { [Op.not] : null }}
                        ]
                    }
                });

                // let query = `select * from meeting_invites where meeting_id in (${meetingId[0].id}) 
                // and emailsent_date is not null`;
                
                // let isCertTriggered = await pool.query(query);
                console.log("details are : " , isCertTrig);
    
                if(isCertTrig.length > 0) {
                    return {isSent : true , meetingId : meetingDetails[0].meeting_id };
                } else return {isSent : false , meetingId : meetingDetails[0].meeting_id };
            } else {
                return {isSent : false , meetingId : null };
            }
           
        } catch(error) {
            console.log('Error Occurred while getting existing meeting data for iCalUid : ' , iCalUid, " : ", error);
            return {isSent : false , meetingId : 0 };
        }
    }

    async getCancelMeetingDetailsWithNullCheck(meetingId) {
        let meeting = await meetingModel.getCancelMeetingDetailsWithNullCheck(meetingId);
        return meeting
    }
}

module.exports = new MeetingRepository();