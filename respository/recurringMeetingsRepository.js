const recurringMeetings =  require('../models/index').sequelize.model('recurring_meetings');
const Sequelize=require('sequelize');
const Op = Sequelize.Op;


// const constants = require('../config/constants');

class RecurringMeetingRepository {

    async getRecurringMeeting(rmId) {
        console.log("---------------Getting Existing REcurring Meeting -------------");
        try {
            let result = await recurringMeetings.findOne({ where : { outlook_meeting_id : rmId }});
            if(result !== null && result !== undefined ) {
                console.log("Result is --------'",result.dataValues.recurring_meeting_id);
                return result.dataValues;
            } else {
                return null;
            }
        } catch(error) {
            console.log("Error Occurred while finding RM Data ");
            return null;
        }
        
    }

    async saveRecurringMeetings(recurringMeetingDetails) {
        try {
            console.log(`----------------Saving records in RM Table with subject : ${recurringMeetingDetails.subject}----------------`);
            let result = await recurringMeetings.create(recurringMeetingDetails);
            console.log("------RM  IS : --------", result.get('recurring_meeting_id'))
            return result.get('recurring_meeting_id');
        }
        catch(error) {
            console.log("Error occurred while saving rm data ----------", error.errors);
            return null;
        }
    }

    async updateRecurringMeetings(recurringMeetingDetails, iCalUId) {
        console.log("----------------Updating Recurring Meeting with Subject : ------------", recurringMeetingDetails.subject);
        try {
            let result = await recurringMeetings.update(
                {
                    subject :recurringMeetingDetails.subject,
                    is_all_day : recurringMeetingDetails.is_all_day,
                    start_date : recurringMeetingDetails.start_date,
                    end_date : recurringMeetingDetails.end_date,
                    repeat_every : recurringMeetingDetails.repeat_every,
                    frequency_type : recurringMeetingDetails.frequency_type,
                    day_index : recurringMeetingDetails.day_index,
                    weekdays_selected : recurringMeetingDetails.weekdays_selected,
                    organizer : recurringMeetingDetails.organizer,
                    TimeZoneName : recurringMeetingDetails.TimeZoneName,
                    TimeZoneOffset : recurringMeetingDetails.TimeZoneOffset,
                    countable : recurringMeetingDetails.countable,
                    // resource : recurringMeetingDetails.resource,
                    next_meeting_date : recurringMeetingDetails.next_meeting_date
                },
                {
                    where: {outlook_meeting_id: iCalUId},
                    returning: true,
                    plain: true
                }
                );

            console.log(result);
            let rowData = await recurringMeetings.findOne({ where: {outlook_meeting_id: iCalUId } });
            // console.log(rowData);
            if(rowData !== null && rowData !== undefined ) {
                console.log("----------------Result after updating ------------ ", rowData.dataValues.recurring_meeting_id);
                return rowData.dataValues;
            } else {
                return null;
            }
            
        }
        catch(error) {  
            console.log("Error Occurred", error.errors);
        }
    }

    async checkIfRm(resourceId) {
        console.log("----------------Checking if this is a recurring meeting or not ---------------");
        try {
            let result = await recurringMeetings.findOne({ where : {resource_id : resourceId }});
            if(result !== null && result !== undefined ) {
                console.log("Result is --------'",result.dataValues.recurring_meeting_id);
                return result.dataValues;
            } else {
                return null;
            }
        }
        catch(error) {
            console.log("Error Occurred while checking if it is a rm ", error);
        }
    }

    async cancelRecurringMeeting(where) {
        // console.log("----------------Cancelling recurring meeting ---------------");
        let rm = await recurringMeetings.update({
            cancelled_at : new Date().toISOString()
        }, where );
        // let rm = await recurringMeetings.destroy(where);
        return rm
    }

}

module.exports = new RecurringMeetingRepository();