const meetings = require('../models').meetings;
const sequelize = require('sequelize')
const db =  require('../models/index');
const Sequelize=require('sequelize');
const Op = Sequelize.Op;
const userModel = require('../models').user;

class UserRepository {

    userDetailsByEmailId(emailId) {

        return new Promise((resolve , reject) => {
            db.sequelize.model('user').findAll({
                where: sequelize.where(sequelize.fn('lower', sequelize.col('email')), emailId.toLowerCase())
        }).then(result => {
            console.log("Successfully retrieved data");
            resolve(result);
        }).catch(error => {
            console.log("Error retrieving user data");
            reject(error);
        })

    })

    }

    async organizationSettingsById(userId) {

        let userSettings = await userModel.getOrgSettings(userId);
        return userSettings;

    }
}

module.exports = new UserRepository();

