const timeZoneDetails =  require('../models/index').sequelize.model('time_zone_details');
const Sequelize=require('sequelize');
const Op = Sequelize.Op;


// const constants = require('../config/constants');

class TimeZoneDetailsRepository {

    async getTimeZoneOffsetValue(timeZoneName) {
        let timeZoneOffset = await timeZoneDetails.findOne({ 
            attributes : ['time_zone_offset'],
            where : { time_zone_name : timeZoneName }
        });
        if (timeZoneOffset && timeZoneOffset['dataValues'] !== undefined) {
            
        return timeZoneOffset.getDataValue('time_zone_offset');
        } else {
            return "+0";
        }
        
    }
    
}

module.exports = new TimeZoneDetailsRepository();