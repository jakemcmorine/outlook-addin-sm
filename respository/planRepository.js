const Sequelize = require("sequelize")
const userPlansModel = require("../models").user_plans;
const model = require("../models");

class planRepository {
  

  async getPlanDetailsByOrg(id) {
    try {
      let result = [];
      let plan = await userPlansModel.planDetailsByOrg(id);
      for (let key in plan) {
        result.push(plan[key].feature_id)
      }
      return result;
    } catch (error) {
      throw error;
    }
  }

  async getPlanDByOrg(org_id) {
    try {
      let plan = await userPlansModel.findOnePlan(org_id);
      return plan;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = new planRepository();
