var fs        = require('fs');
var path      = require('path');
var basename  = path.basename(__filename);
var redis_models    = {};
var redis = require("redis");
    //client = redis.createClient();
var client;
if(process.env.REDIS_HOST != ''){
    console.log(process.env.REDIS_HOST);
    client = redis.createClient({host:process.env.REDIS_HOST, port:process.env.REDIS_PORT });
   
    const {promisify} = require('util');
    
    client.on("error", function (err) {
        console.log("Error conecting to redis server");
    });
    client.on("connect", function (success) {
        console.log("connected to redis server");
    });
    
    // End of Mongoose Setup
}else{
    console.log("No redis Credentials Given");
}

module.exports = client;
