// require('dotenv').config();
const dotenv = require('dotenv');
const express = require('express');
const fs = require('fs');
const https = require('https');
const http  = require('http');
const app = express();
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const redis = require('redis');
const session = require('express-session');
const cors = require('cors');
const axios = require('axios');
const methodOverride = require('method-override');
const querystring = require('querystring');

console.log(process.env.NODE_ENV);
process.env.NODE_ENV = process.env.NODE_ENV || "local";
if (process.env.NODE_ENV === 'local') {
    dotenv.config({ path: '.env-local' });
} else if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'dev') {
    dotenv.config({ path: '.env-development' });
}
else if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'prod') {
    dotenv.config({ path: '.env-production' });
}
console.log(process.env.NODE_ENV);
console.log(process.env.CLIENT_SECRET);

const passportModule = require('./modules/PassportModule');
const authModule = require('./modules/AuthModule');
const { EnsureAuth } = require('./modules/EnsureAuth');
const webhooks = require('./modules/Webhooks');
var meetingsRouter = require('./routes/MeetingRoutes');
var userRouter = require('./routes/UserRoutes');

var Redis = require('./redis/');
var RedisClient = require('./redis/redisModel');
RedisClient = new RedisClient(Redis);

require('./config/MySQLPool');
const mysql = require('./config/MySQLPool');

const redisClient = redis.createClient();
const RedisStore = require('connect-redis')(session);

if (process.env.NODE_ENV === "local") {
    https.createServer({
        key: fs.readFileSync('server.key'),
        cert: fs.readFileSync('server.cert')
    }, app).listen(3030, () => console.info('server is listening on https://localhost:3030'));
} else if (process.env.NODE_ENV === "development") {
    https.createServer({
        key: fs.readFileSync('/etc/letsencrypt/live/dev-outlook-addin-ss.emvigotechnologies.com/privkey.pem'),
        cert: fs.readFileSync('/etc/letsencrypt/live/dev-outlook-addin-ss.emvigotechnologies.com/cert.pem')
    }, app).listen(3030, () => console.info('server is listening on port 3030. https://dev-outlook-addin-ss.emvigotechnologies.com'));
} else if (process.env.NODE_ENV === "production") {
    http.createServer(app).listen(3030, () => console.info('server is listening on port 3030. https://outlook-addin-meetings.sustainably.run'));
}

app.set('trust proxy', true);
app.use(function (req, res, next) {
    if (req.headers['x-arr-ssl'] && !req.headers['x-forwarded-proto']) {
        req.headers['x-forwarded-proto'] = 'https';
    }
    return next();
});

// app.use(cors({
//     origin: (origin, callback) => {
//         callback(null, true);
//     }
// }))

app.use(cors({
    credentials: true
}))
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use(methodOverride());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(session({
    secret: 'session secret',
    resave: false,
    saveUninitialized: false,
    cookie: { secure: true, sameSite : 'none' },
    store: new RedisStore({
        port: process.env.REDIS_PORT,
        client: redisClient,
        ttl: 2592000,
    })
}));
app.use(passportModule);
app.set('view engine', 'pug');
app.use('/auth', authModule);
app.use('/', express.static('dist'));
app.use('/events', webhooks);
app.use('/meetings', meetingsRouter);
app.use('/user', userRouter);

app.get('/start', (req, res) => {
    res
        .status(200)
        .set("Content-Type", "text/html")
        .render('start_screen', {})
})

app.get('/dialog', (req, res) => {
    res
        .status(200)
        .set("Content-Type", "text/html")
        .render('login_success', {})
})

const jsonfile = require(__dirname + '/microsoft-identity-association.json');
app.get('/.well-known/microsoft-identity-association.json', (req, res) => {
    // res.header("Content-Type", 'application/json');
    // res.json(jsonfile);

    res.writeHeader(200, { 'Content-Type': 'application/json', 'Content-Length': JSON.stringify(jsonfile).length + '' })
    res.write(JSON.stringify(jsonfile))
    res.end()
})

app.get('/.well-known/microsoft-identity-association', (req, res) => {
    res.writeHeader(200, { 'Content-Type': 'application/json', 'Content-Length': JSON.stringify(jsonfile).length + '' })
    res.write(JSON.stringify(jsonfile))
    res.end()
})

app.post('/check', EnsureAuth, async (request, response, next) => {
    try {
        // const email = request.user._json.preferred_username;
        let email = request.body.email;
        if(!email){
            email = request.user._json.preferred_username;
        }
        console.log(`email : ${email}`);

        let responseCache = await RedisClient.getAsync(email);
        console.log(responseCache);
        if (responseCache && responseCache.message) {
            responseCache = JSON.parse(responseCache)
            response.status(responseCache.response_code || 200)
                .json({
                    message: responseCache.message,
                });
        }else{
            await RedisClient.setAsync(email, JSON.stringify({}));

            if (!email) {
                throw new Error('email is empty');
            }

            console.log("userAccess=========");
            let userAccess = await checkUserAccess(request.user._json.preferred_username);
            console.log(userAccess);
            if(!userAccess){
                return response.status(401)
                    .json({
                        message: 'UNAUTHORIZED',
                        data: ""
                    });
            }

            const info = await axios.post(process.env.LAMBDA_BASE_URL + '/info-web', {
                Emails: [email]
            });
    
            console.log("============");
            // console.log(info);
    
            const error = ['not_invited', 'orphan_user'].some((message) => {
                return message === info.data.message;
            });
    
            const status = error ? 403 : 200;

            await RedisClient.setAsync(email, JSON.stringify({ response_code: status, message: info.data.message }));
            await RedisClient.expire(email, 30);
    
            response.status(status)
                .json({
                    message: info.data.message,
                });
        }

            
    } catch (error) {
        console.error(error);
        response.status(401)
            .json({
                message: 'UNAUTHORIZED',
                data:""
            });
    }
});



async function checkUserAccess(email) {
    try{
        let client_secret = process.env.CLIENT_SECRET;
        let sql = `SELECT accessToken,refreshToken FROM user_tokens 
                    WHERE email = ?`
        let result = await mysql.query(sql, email);
        let userData = result[0][0];
        if (!userData) throw new Error(`no user found with id ${email}`);
        // let accessToken = userData.accessToken;
        let refresh_token = userData.refreshToken;
        let redirect_uri = process.env.REDIRECT_URL;
        const requestBody = {
            client_id: process.env.CLIENT_ID,
            refresh_token: refresh_token,
            redirect_uri: redirect_uri,
            grant_type: 'refresh_token',
            client_secret: client_secret
        };
    
        const axiosConfig = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }
    
        const graphResponse = await axios.post('https://login.microsoftonline.com/common/oauth2/v2.0/token',
            querystring.stringify(requestBody), axiosConfig);
    
        if (graphResponse.status === 200) {
            const tokenData = graphResponse.data;
            // console.log(tokenData);
            sql = `UPDATE user_tokens SET accessToken = ?, refreshToken = ? WHERE email =? `;
            await mysql.execute(sql, [tokenData.access_token, tokenData.refresh_token, email])
            return true;
        } else {
            return false;
        }
    }catch(error){
        if(error.response){
            console.log(error.response.data);
        }
        return false;
    }
}
