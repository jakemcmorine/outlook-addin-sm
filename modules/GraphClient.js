const { ImplicitMSALAuthenticationProvider } = require('@microsoft/microsoft-graph-client/lib/src/ImplicitMSALAuthenticationProvider');
const { Client } = require('@microsoft/microsoft-graph-client');
const { UserAgentApplication } = require('msal');
const { MSALAuthenticationProviderOptions } = require('@microsoft/microsoft-graph-client/lib/src/MSALAuthenticationProviderOptions');

const msalConfig = {
  auth: {
    clientId: process.env.APPLICATION_ID,
    redirectUri: process.env.REDIRECT_URL
  }
};

const graphScopes = [
  'openid',
  'offline_access',
  'profile',
  'OnlineMeetings.ReadWrite',
  'Calendars.ReadWrite',
  'People.Read.All'
];

const msalApplication = new UserAgentApplication(msalConfig);
const options = new MSALAuthenticationProviderOptions(graphScopes);
const authProvider = new ImplicitMSALAuthenticationProvider(msalApplication, options);

module.exports = Client.initWithMiddleware({
  authProvider: authProvider
});
