const mysql = require('../config/MySQLPool');
exports.EnsureAuth = async (req, res, next) => {
    let isDesktopPluginUser = false;
    try {
        console.log(req.body);
        if (req.body.email) {
            let result = await mysql.query(`SELECT email FROM plugin_status 
                WHERE email = ? AND plugin_type = ?`, [req.body.email, "outlookDesktop"]);
            if(result[0].length){
                console.log(result[0]);
                isDesktopPluginUser = true;
            }
        }
    } catch (error) {
        console.log(error);
    } finally {
        console.log('EnsureAuth: ' + req.url);
        if (req.isAuthenticated()) { return next(); }
        // res.redirect('/?status=denied');
        res
            .status(401)
            .json({
                message: 'UNAUTHORIZED',
                data: { isDesktopPluginUser }
            })
    }
    
}