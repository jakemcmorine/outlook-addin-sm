const express = require('express');
const passport = require('passport');
require('isomorphic-fetch');
const { Client } = require('@microsoft/microsoft-graph-client');
const { OIDCStrategy, BearerStrategy } = require('passport-azure-ad');
const passportModule = express.Router();
const axios = require('axios');
const moment = require('moment');

var Redis = require('../redis/');
var RedisClient = require('../redis/redisModel');
RedisClient = new RedisClient(Redis);

const mysql = require('../config/MySQLPool');

passport.serializeUser(function (user, done) {
    done(null, user.oid);
});

passport.deserializeUser(function (oid, done) {
    findByOid(oid, function (err, user) {
        done(err, user);
    });
});

// array to hold logged in users
var users = [];

var findByOid = async function (oid, fn) {
    users = await RedisClient.lrange("passport_users", 0, -1)
    for (var i = 0, len = users.length; i < len; i++) {
        // var user = users[i];
        var user = JSON.parse(users[i]);
        // console.info('we are using user: ', user);
        if (user.oid === oid) {
            return fn(null, user);
        }
    }
    return fn(null, null);
};

let env = process.env.NODE_ENV;
let logLevel = "info";
let redirectUrl = process.env.REDIRECT_URL
console.log("Redirect URL: " + redirectUrl);
const azureOpenIDStrategy = new OIDCStrategy({
    identityMetadata: "https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration",
    clientID: process.env.CLIENT_ID,
    responseType: "code id_token",
    responseMode: 'form_post',
    redirectUrl: redirectUrl,
    allowHttpForRedirectUrl: false,
    clientSecret: process.env.CLIENT_SECRET,
    isB2C: false,
    validateIssuer: false,
    issuer: null,
    passReqToCallback: false,
    useCookieInsteadOfSession: false,
    cookieEncryptionKeys: [
        { 'key': '12345678901234567890123456789012', 'iv': '123456789012' },
        { 'key': 'abcdefghijklmnopqrstuvwxyzabcdef', 'iv': 'abcdefghijkl' }
    ],
    scope: [
        'openid',
        'offline_access',
        'profile',
        'OnlineMeetings.ReadWrite',
        'Calendars.ReadWrite',
        'People.Read.All'
    ],
    prompt:"admin_consent",
    loggingLevel: logLevel,
    nonceLifetime: null,
    nonceMaxAmount: 5,
    clockSkew: null
}, async function (iss, sub, profile, jwtClaims, accessToken, refreshToken, params, done) {
    if (!profile.oid) {
        return done(new Error("No oid found"), null);
    }
    const profile_data = JSON.parse(profile._raw);
    const email = profile_data.preferred_username;
        axios.post(process.env.LAMBDA_BASE_URL + '/info-web', {
        Emails: [email]
    }).then(async function (response) {
        console.info('plugin info called');
        const error = ['not_invited', 'orphan_user'].some((message) => {
            return message === response.data.message;
        });
        const status = error ? 403 : 200;
        await RedisClient.setAsync(email, JSON.stringify({ response_code: status, message: response.data.message }));
        await RedisClient.expire(email, 30);
    }).catch(function (error) {
        console.error(error)
    })

    // expiration time
    const expiration = moment().add(4230, 'minutes')

    const calendarSubscription = {
        changeType: "created,updated",
        notificationUrl: process.env.BASE_URL + "/events/notify",
        resource: `users/${email}/events`,
        expirationDateTime: expiration.utc(),
        // includeResourceData: true,
    }

    const graph = Client.initWithMiddleware({
        authProvider: {
            getAccessToken: async () => accessToken
        }
    });

    try {
        // let deletedhook = await graph.api(`/subscriptions/7c558eac-bdd9-4e3d-a676-c558d5a8de45`).delete();
        
        let webhook;
        let webhookRecieved = await graph.api('/subscriptions').get();
        console.log(webhookRecieved);

        if(webhookRecieved.value.length !== 0) {
            let userWebHooks = webhookRecieved.value.filter(x =>  x['resource'].includes(email) && x['notificationUrl'].includes(process.env.URL_KEY) );
            console.log("user webhooks before : ", userWebHooks);
            
            if(userWebHooks.length > 1) {
                for(let i =1 ; i< userWebHooks.length ; i++) {
                    let deletedhook = await graph.api(`/subscriptions/${userWebHooks[i].id}`).delete();
                }
                webhook = userWebHooks[0];
            } 
            else if(userWebHooks.length == 0) {
                webhook = await graph.api('/subscriptions').post(calendarSubscription);
            } else if(userWebHooks.length == 1) {
                webhook = userWebHooks[0];
            }
        } else {
            webhook = await graph.api('/subscriptions').post(calendarSubscription);
        }
            
        
        await mysql.execute("INSERT INTO user_tokens (email, accessToken, refreshToken, webhookId," +
            " subscriptionTime) VALUES(?, ?, ?, ?, ?)" +
            " ON DUPLICATE KEY UPDATE email = VALUES(email), accessToken = VALUES(accessToken)," +
            " refreshToken = VALUES(refreshToken), webhookId = VALUES(webhookId)," +
            " subscriptionTime = VALUES(subscriptionTime)", [
            profile_data.preferred_username,
            accessToken,
            refreshToken,
            webhook.id,
            moment().format("YYYY/MM/DD HH:mm:ss")
        ]);
    } catch (error) {
        console.error('failed to create webhook')
        console.error(error)
    }

    process.nextTick(function () {
        findByOid(profile.oid, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                // "Auto-registration"
                // users.push(profile);
                RedisClient.lpush("passport_users", JSON.stringify(profile));
                return done(null, profile);
            }
            return done(null, user);
        });
    })
});

const azureBearerStrategy = new BearerStrategy({
    identityMetadata: "https://login.microsoftonline.com/marvinsustainablemeeting.onmicrosoft.com/v2.0/.well-known/openid-configuration",
    clientID: process.env.CLIENT_ID,
    validateIssuer: true,
    passReqToCallback: false,
    issuer: null,
    isB2C: false,
    policyName: null,
    loggingLevel: 'info',
    scope: [
        'openid',
        'offline_access',
        'profile',
        'OnlineMeetings.ReadWrite',
        'Calendars.ReadWrite',
        'People.Read.All'
    ]
}, function (token, done) {
    console.info('verifying the user');
    console.info(token, 'was the token retreived');
    findById(token.oid, function (err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
            // "Auto-registration"
            log.info('User was added automatically as they were new. Their oid is: ', token.oid);
            users.push(token);
            owner = token.oid;
            return done(null, token);
        }
        owner = token.oid;
        return done(null, user, token);
    });
});

passportModule.use(passport.initialize());
passportModule.use(passport.session());
passport.use(azureOpenIDStrategy);
passport.use(azureBearerStrategy);


module.exports = passportModule;