const events = require('express').Router();
require('isomorphic-fetch');
const { Client, GraphError } = require('@microsoft/microsoft-graph-client');
const querystring = require('querystring');
const axios = require('axios');
const mysql = require('../config/MySQLPool');
const meetingController = require('../controllers/MeetingController');
const rmController = require('../controllers/RecurringMeetingController');

var Redis 			=  require('../redis/');
var RedisClient 	 = require('../redis/redisModel');
RedisClient= new RedisClient(Redis);

events.post('/notify', async (req, res) => {
  const env = process.env.NODE_ENV;
  let redirect_uri = process.env.REDIRECT_URL;
  let client_secret = process.env.CLIENT_SECRET;
  if (req.query && req.query.validationToken) {
    res.status(200).send(req.query.validationToken);
  } else {
    const messages = req.body.value;
    for (const message of messages) {
      // console.log(`--------Message is :  -----------`, message);
      try {
        // console.log("message subscription id : ", message.subscriptionId);
        // console.log(JSON.stringify(message));
        const sql = `SELECT u.id, u.email, tokens.webhookId, tokens.accessToken,
          tokens.refreshToken, tokens.subscriptionTime FROM user_tokens tokens
          LEFT JOIN users u ON tokens.email = u.email
          WHERE tokens.webhookId = ?`
        let result = await mysql.query(sql, message.subscriptionId);
        let userData = result[0];
        userData = userData[0];
        if (!userData) throw new Error(`no webhook id saved for ${message.subscriptionId}`);
        let accessToken = userData.accessToken;
        let graph = Client.initWithMiddleware({
          authProvider: {
            getAccessToken: async () => accessToken
          },
        });
        let calendarResource;
        try {
         
          calendarResource = await graph.api(message.resource).get();

        } catch (error) {
          if (error instanceof GraphError) {
            switch (error.code) {
              case "ErrorItemNotFound":
                /** let isRm = rmController.checkIfRm(message.resourceData.id); if(isRm) { } else {  } */
                  rmController.cancelRecurringMeeting(message.resourceData.id);
                  meetingController.cancelMeeting(message.resourceData.id);
                break;
              case "InvalidAuthenticationToken":
                try {
                  accessToken = await refreshToken(userData.email, userData.refreshToken, redirect_uri, client_secret);
                  graph = Client.initWithMiddleware({
                    authProvider: {
                      getAccessToken: async () => accessToken
                    }
                  })
                  calendarResource = await graph.api(message.resource).get();
                } catch (error) {
                  console.error("======= GraphError");
                  if(error.data !== undefined) {
                    console.log(`Error: ` , error.data)
                  } else {
                    console.error(error);
                    throw error;
                  }
                }
                break;
            }
          } else {
            console.error("=======Not a GraphError");
            console.error(error);
            throw error;
          }
        }
        // const location = calendarResource.location.displayName || null;
        const userId = userData.id;
        
        if (calendarResource) {
          if (!calendarResource.isOrganizer) throw new Error(`different meeting organizer`);
          
          switch (message.changeType) {
            case "created":

              /**
               * changeKey provided in calendarResource by graph api indicates the change in event data. 
               * Many times , we receive multiple notifications for same event and to prevent the execution of code unnecessarily
               * we use changeKey to identify.
               * Any future changes in the same resource(event) , it will have a different changeKey and the same will replace the previous one.
               */
              await RedisClient.setAsync(calendarResource.changeKey , calendarResource.changeKey);
              await RedisClient.expire(calendarResource.changeKey, 60);
              // console.log("------------  During Creation Change Key : " ,await RedisClient.getAsync(calendarResource.changeKey) + " For Cal UID : ", calendarResource.iCalUId);
              
              if (calendarResource['recurrence'] !== null) {
                rmController.saveRecurringMeetingData(calendarResource, "created", message, userId, graph);
              } else {
                meetingController.createMeeting(calendarResource, userId);
              }
              break;

            case "updated":
              /**
                * During creation . Microsoft also sends updating notification for same resource(event) To avoid executing following 
                * piece of code , we check if the changeKey is same.If the changeKey is same , it means there are no changes and thus we dont need to update 
                * or execute the code again and again, */

              let changeKey = await RedisClient.getAsync(calendarResource.changeKey);
              console.log("If change key exists : " , changeKey);
              if(!changeKey) {

                await RedisClient.setAsync(calendarResource.changeKey , calendarResource.changeKey);
                await RedisClient.expire(calendarResource.changeKey, 60);

                console.log("------------ During Updation Change Key : -----------", calendarResource.changeKey, "--------- outlook_meeting_id ", calendarResource.iCalUId);

                if (calendarResource['recurrence'] !== null) {
                   rmController.saveRecurringMeetingData(calendarResource, "updated", message, userId, graph);
                } else {
                  // TODO: manage meeting updation
                  meetingController.updateMeeting(calendarResource);
                }
              } else {
                console.log("Same Data Received ");
              }
              break;
          }
        }
      } catch (error) {
        console.error(error.message);
      } finally {
        res.status(202).send();
      }

    }
  }
});

async function refreshToken(email, refresh_token, redirect_uri, client_secret) {
  const requestBody = {
    client_id: process.env.CLIENT_ID,
    refresh_token: refresh_token,
    redirect_uri: redirect_uri,
    grant_type: 'refresh_token',
    client_secret: client_secret
  };

  const axiosConfig = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }
  
  const graphResponse = await axios.post('https://login.microsoftonline.com/common/oauth2/v2.0/token',
    querystring.stringify(requestBody), axiosConfig);
  
  if (graphResponse.status === 200) {
    const tokenData = graphResponse.data;
    const sql = `
    UPDATE user_tokens SET accessToken = ?, refreshToken = ? WHERE email = ?
    `;
    await mysql.execute(sql, [tokenData.access_token, tokenData.refresh_token, email])
    return tokenData.access_token;
  } else {
    throw new Error(`failed to refresh access token for ${email}`);
  }
}

module.exports = events;
