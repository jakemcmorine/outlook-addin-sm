var randomstring    = require("randomstring");
var Redis 			=  require('../redis/');
var RedisClient 	 = require('../redis/redisModel');

RedisClient= new RedisClient(Redis);

const createSession = async function(key) {
    try {
        let success = await RedisClient.hmset(key,"val",key);
        return key;
    } catch(error) {
        throw error;
    }
}
module.exports.createSession = createSession;

const tokenExpireTime = async function(key,expireInSeconds){//returns token
    try {
        let success = await RedisClient.expire(key, expireInSeconds);
        return success;
    } catch (error) {
        throw error;
    }
}
module.exports.tokenExpireTime = tokenExpireTime;

const deleteSession = async function(key) {//returns token
    [error, success] = await to(RedisClient.del(key));
    if (success) {
       return key;
    } else {
      console.log(error);
    }     
}
module.exports.deleteSession = deleteSession;

const findSession = async function(key){//returns token
    [error,vals]= await to(RedisClient.hgetall(key));
    if(vals){
        return vals;
    }else{
        return error;
    }
}
module.exports.findSession = findSession;