const express = require('express');
const passport = require('passport');
const auth = express.Router();

auth.get('/', (req, res, next) => {
    // console.log(req)
    passport.authenticate('azuread-openidconnect', {
        response: res,
        failureRedirect: '/?status=denied'
    })(req, res, next);
}, (req, res) => {
    console.info('Login called');
    res.send('');
});

auth.use('/openid/return', function (req, res, next) {
    // console.log(req.body.error);
    if(req.body.error == 'access_denied' || req.body.hasOwnProperty('error')) {
        console.log("Denying")
        res.redirect('/?status=denied');
    } else {
        passport.authenticate('azuread-openidconnect', {
            response: res,
            failureRedirect: '/?status=denied',
        })(req, res, next);
    }
}, (req, res) => {
    console.info('Received a return from AzureAD');     
    console.log("Success");
    try {
        console.log(req.isAuthenticated());
    }catch(error) {
        console.log(error);
    }
    res.cookie('sm', 'this will be set in your browser');
    res.cookie('SameSite', 'None', { expires: new Date(Date.now() + 900000) });
    res.redirect('/?status=success');
    // res.render('login_success', {});
});

module.exports = auth;