import { Injectable, NgZone } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import axios from 'axios';
import { AuthService } from './auth-service';

@Injectable({
    providedIn: 'root'
  })
export class AuthGuardService implements CanActivate {

  constructor(public router: Router, private authService : AuthService, private ngZone : NgZone) {}

  canActivate(): Promise<boolean> {
    let isAuthUrl = "/check";

    if (window.location.href.indexOf('status') !== -1) {
      this.handleDialogClose();
    } else {
      if (!this.authService.getIsLoggedIn()) {

        return new Promise(result => {
          Office.onReady(() => {
            let email = Office.context.mailbox.userProfile.emailAddress
            axios.post(isAuthUrl, { withCredentials: true, email: email }).then(res => {
              if (res) {
                console.log("Response received is : ", res.data);
                if (res.data['message'] == "SUCCESS") {
                  this.authService.setIsLoggedIn(true);
                  result(true);

                } else {
                  console.log("Redirecting to Activate Page");
                  this.authService.setIsLoggedIn(false);

                  this.router.navigate(['/activate']);
                  result(false);

                }
              }
            }).catch(err => {
              this.authService.setIsLoggedIn(false);
              console.log("Response Status is  : ", err.response.status, " Response Data is : ", err.response.data);
              switch (err.response.status) {
                case 401:
                  debugger
                  console.log("Redirecting to Activate Page");
                  if (err.response.data.data && err.response.data.data.isDesktopPluginUser) {
                    this.authService.setIsLoggedIn(false);
                    this.router.navigate(['/activate/desktopuser']);
                    result(false);
                  } else {
                    this.ngZone.run(() => {
                      this.router.navigate(['/activate']);
                      result(false);
                    });
                  }
                  break;
                case 403:
                  console.log("Redirecting to New User Page");
                  this.authService.setUserType(err.response.data.message);
                  this.router.navigate(['/new-user']);
                  result(false);
                  break;
              }
            });
          })
        })
      }
      else {
        return new Promise(result => {
          if (window.location.href.indexOf('status=denied') !== -1) {
            console.log("its denying from can activate");
            this.router.navigate(['/activate']);
            Office.context.ui.messageParent(false);

          }
          if (window.location.href.indexOf('status=success') !== -1) {
            console.log("its Success from can activate");
            this.router.navigate(['/']);
            Office.context.ui.messageParent(true);
          }
          switch (this.authService.getUserType()) {
            case 'orphan_user':
              this.router.navigate(['/new-user']);
              result(false);
              break;
            case 'not_invited':
              this.router.navigate(['/new-user']);
              result(false);
              break;
            default:
              console.log("Returning as user is logged in");
              result(true);
              break;
          }

        });
      }
    }
  }

  handleDialogClose() {
    if(window.location.href.indexOf('status=denied') !== -1) {
      console.log("its denying from can activate 1");
      Office.context.ui.messageParent(false);

    } 
    if(window.location.href.indexOf('status=success') !== -1)  {
      console.log("its Success from can activate 1");
      Office.context.ui.messageParent(true);

    }
  }
 }