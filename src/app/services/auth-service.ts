import { Injectable } from '@angular/core';


@Injectable({
    providedIn: 'root'
  })
export class AuthService {
    emailId : string;
    isLoggedIn : boolean = false;
    userType : string;
    
    setIsLoggedIn(value) {
        this.isLoggedIn = value;
    }

    getIsLoggedIn() {
        return this.isLoggedIn;
    }

    getUserType() {
        return this.userType;
    }

    setUserType(userType) {
        this.userType = userType;
    }

    constructor() {
        // this.emailId = Office.context.mailbox.userProfile.emailAddress;
    }

    getUserEmailId() {
        return this.emailId;

    }

}