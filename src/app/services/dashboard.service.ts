import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx'
import axios from 'axios';
import { environment } from 'src/environments/environment'
import { Router } from '@angular/router';
import { AuthService } from './auth-service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const SAVE_CHECK_URL = "https://25805594d974.ngrok.io/saveCheck";
const GET_CHECK_URL = "https://25805594d974.ngrok.io/getCheck?email=";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  
  pluginLoginApi = environment.pluginLoginApi; //Based on Environment during build it will provide the pluginLoginApi
  
  // constructor(
  //   private http: Http,
  // ) { }
  constructor(private http: Http,public router: Router, private authService: AuthService) { }


  //Get user details
  getUserDetails(emailId) {
    // const headers = new Headers();
    console.log("Calling user details api");
    let url = `/user/${emailId}`;
    return new Promise((resolve, reject) => {
      axios.get(url).then(res => {
        console.log(res);
        resolve(res);
      }).catch(err => {
        reject(err);
      })
    })
     

  }


  //Get count details
  getCountDetails(data) {
    const headers = new Headers();
    return this.http.post(`/meetings/count`, data, { headers: headers })
      .map(res => res.json())
      .map((resp: Response) => {
        return resp;
      }).catch((error: any) => Observable.throw('Server Error'));
  }

  //passing check box value
  passCheckBoxValue(data) {
    const headers = new Headers();
    return this.http.post(`https://25805594d974.ngrok.io/saveCheck`, data, { headers: headers })
      .map(res => res.json())
      .map((resp: Response) => {
        return resp;
      }).catch((error: any) => Observable.throw('Server Error'));
  }

  saveToggleEvent(data) {

       return  axios.post(SAVE_CHECK_URL, data)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            console.log("ERROR occurred while saving data : ", error);
        })
  }

  getCheckDetails() {
    let finalUrl = GET_CHECK_URL+Office.context.mailbox.userProfile.emailAddress;
    return axios.get(finalUrl).then(response => {
      return response.data;
    })
    .catch(error => {
      console.log("Error Occurred while getting details");
    })

  }

  //Get Login token
  getLoginToken(data) {
    let params = { userId : data.userId };
    let finalUrl = `/user/getToken`;
    return axios.post(finalUrl, params).then(response => {
      return response.data;
    })
    .catch(error => {
      console.log("Error Occurred while getting LoginToken");
    })
  }

  logoutUser(email) {
    let params = { email: email };
    let finalUrl = `/user/logout`;
    return axios.post(finalUrl, params).then(response => {
      console.log(response);
      if (response.status === 200){
        console.log("Redirecting to Activate Page");
        this.authService.setIsLoggedIn(false);
        this.router.navigate(['/activate']);
        return true
      }else{
        console.log("Error Occurred while Logout API");
        return false;
      }
    })
      .catch(error => {
        console.log("Error Occurred while Logout");
      })
  }
}
