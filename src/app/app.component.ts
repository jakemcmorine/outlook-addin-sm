import { Component } from '@angular/core';
import { NgZone } from '@angular/core';

// declare const Excel: any;
declare const Office: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
// export class AppComponent {
//   colors = ['red', 'blue', 'yellow'];
//   color = 'red';

//   onChangeColor(color: string) {
//     this.color = color;
//   }

//   onColor() {
//     Excel.run(async (context) => {
//       const range = context.workbook.getSelectedRange();
//       range.format.fill.color = this.color;
//       await context.sync();
//     });
//   }
// }

export class AppComponent {
  constructor(private zone: NgZone) { }

  
  title = "WELCOME DEAR.......";

  ngOnInit() {
    console.log("In App component")
    if(window.location.href.indexOf('status=denied') !== -1) {
      console.log("its denying");
      Office.context.ui.messageParent(false);

    } 
    if(window.location.href.indexOf('status=success') !== -1)  {
      console.log("its Success");
      Office.context.ui.messageParent(true);

    }
  }

  // myFunction() {
  //   this.zone.run(() => {
  //     console.log(" successfully workingggg.......")
  //   });
  // }

}
