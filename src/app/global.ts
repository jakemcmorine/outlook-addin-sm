export const globalConstants = Object.freeze({

    SM_USER : "smUser",
    ORPHAN_USER : "orphan_user",
    NEW_USER : "not_invited",
    success : '200',
    notRegistered: '604',
    ORPHAN_USER_MESSAGE : 'Please Sign-Up to subscribe our services',
    NEW_USER_MESSAGE : 'Admin has been notified',
    COOKIE_MESSAGE : "Please enable your cookies to use Sustainably Run Meetings Plug-in",
    DESKTOP_USER_MESSAGE : "Please login to Web Outlook to use Sustainably Run Meetings Plugin"

});