import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActivateScreenComponent } from './components/activate-screen/activate-screen.component';
import { SuccessScreenComponent } from './components/success-screen/success-screen.component';
import { LoaderComponent } from './components/loader/loader.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';
import {  AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { AppComponent } from './app.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { DontInstallComponent } from './components/dont-install/dont-install.component';


// const routes: Routes = [
//   { path: 'dashboard', component: DashboardComponent },
//   { path: 'loader', component: LoaderComponent },
//   { path: 'sucessScreen', component: SuccessScreenComponent },
//   { path: 'dialogBox' , component : DialogBoxComponent},
//   { path: '',  component: MainLayout, canActivate: [AuthGuard]  },
//   { path: 'activate' , component: ActivateScreenComponent }
// ]


const routes: Routes = [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: "full"
    },
    {
      path: '',
      component: AppComponent,
      children: [
        {
          path: 'activate',
          component: ActivateScreenComponent
        },
        {
          path: 'activate/:desktopuser',
          component: ActivateScreenComponent
        },
        {
          path: 'dashboard',
          component: DashboardComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'loader',
          component: LoaderComponent
        },
        {
          path: 'sucessScreen',
          component: SuccessScreenComponent
        },
        {
          path: 'dialogBox',
          component: DialogBoxComponent
        },
        {
          path: 'new-user',
          component: NewUserComponent
        },
        {
          path: 'not-install-user',
          component: DontInstallComponent
        },

      ]
    }
  // { path: 'activate/:status' , component: ActivateScreenComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],

  exports: [RouterModule]
})
export class AppRoutingModule { }
export const RoutingComponents = [AppComponent]