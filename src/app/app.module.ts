import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from "ngx-spinner";
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppRoutingModule , RoutingComponents} from './app-routing.module';
import { AppComponent } from './app.component';

import { ActivateScreenComponent } from './components/activate-screen/activate-screen.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SuccessScreenComponent } from './components/success-screen/success-screen.component';
import { LoaderComponent } from './components/loader/loader.component';
import {DialogBoxComponent}  from './components/dialog-box/dialog-box.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { DontInstallComponent } from './components/dont-install/dont-install.component';

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,
    ActivateScreenComponent,
    DashboardComponent,
    NewUserComponent,
    SuccessScreenComponent,
    LoaderComponent,
    DialogBoxComponent,
    DontInstallComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
