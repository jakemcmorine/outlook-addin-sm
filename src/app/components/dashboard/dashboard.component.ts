import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from './../../services/dashboard.service';
import { ThrowStmt } from '@angular/compiler';
import { Router, ActivatedRoute, ActivationStart, RouterOutlet } from "@angular/router";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user: any
  count: any
  userExist: boolean
  loginUrl : any;

  marked = true;
  theCheckbox = false;

  show: boolean = false;
  @ViewChild(RouterOutlet) outlet: RouterOutlet;

  constructor(
    private dashboardSerice: DashboardService,
    private route: ActivatedRoute, private router : Router
  ) { }

  ngOnInit() {

      console.log(Office.context.mailbox.diagnostics.hostName)
      localStorage.setItem('auth', "true");
      localStorage.setItem(Office.context.mailbox.userProfile.emailAddress , "false");
      // Office.onReady(() => {
        this.getUser();
      // })
        this.router.events.subscribe(e => {
          if (e instanceof ActivationStart && e.snapshot.outlet === "dashboard")
            this.outlet.deactivate();
        });
    }

  getUser() {
    // get user details
    let emailId = Office.context.mailbox.userProfile.emailAddress;

    this.dashboardSerice.getUserDetails(emailId)
    .then((res : any) => {
      this.user = res?.data.length ? res.data[0] : {};
      console.log(this.user);
      this.user['name'] = Office.context.mailbox.userProfile.displayName;
      if (this.user != undefined) {
        console.log("user is not undefined", this.user);
        // get count details
        let data = {
          userId : this.user.id,
          treeNumber : this.user.treeNumber
        }
        this.dashboardSerice.getCountDetails(data).subscribe(res => {
          console.log("Trees and meetings : ", res)
          if(res !== undefined && res !== null) {
            this.count = res
            if (this.count != undefined) {
              this.show = true
            } else {
              this.show = false;
            }
          }
        });
        
        console.log(data);
        this.dashboardSerice.getLoginToken(data).then(res => {
          console.log("Login Token : ", res)
          if(res !== undefined && res !== null) {
            // this.loginUrl = "http://localhost:3003/pluginLogin?token=" + res.tocken
            this.loginUrl = this.dashboardSerice.pluginLoginApi + res.tocken
          }
        });
        // end of count details
      } else {
        this.count = {
          meetingCount : 0,
          meetingInviteCount : 0
        }
        this.show = true;
        console.log("no value")
      }
    }).catch(err => {
      this.user = {};
      this.user['name'] = Office.context.mailbox.userProfile.displayName;
      console.log("Error Occurred while receiv");
      this.count = {
        meetingCount : 0,
        meetingInviteCount : 0
      }
      this.show = true;
    })
    //end of uer details
  }

  // check box value 
  toggleVisibility(e) {
    const data = {
      email : Office.context.mailbox.userProfile.emailAddress,
      checkValue: !this.marked
    }

    localStorage.setItem(Office.context.mailbox.userProfile.emailAddress , `${e.target.checked}`);
  


    //opt-out mail id inject starts here
    var item = Office.context.mailbox.item;	
    var toRecipients, ccRecipients;	
    console.log(item.itemType,Office.MailboxEnums.ItemType.Appointment);	
    // Verify if the composed item is an appointment or message.	
    if (item.itemType == Office.MailboxEnums.ItemType.Appointment) {	
      toRecipients = item.requiredAttendees;	
      ccRecipients = item.optionalAttendees;	
    }	
    else {	
      toRecipients = item.to;	
      ccRecipients = item.cc;	
    }	
    toRecipients.getAsync((asyncResult) =>{	
      let inviteeList = []	
      if (asyncResult.status == Office.AsyncResultStatus.Failed) {	
        console.log(asyncResult.error.message);	
      }	
      else {	
        if (asyncResult.value.length){	
          inviteeList = asyncResult.value;	
        }	
        console.log(inviteeList);	
        if (e.target.checked){	
          inviteeList.push({	
            "displayName": "Optout SR",	
            "emailAddress": "opt-out@sustainably.run"	
          });	
        } else{	
          inviteeList = inviteeList.filter(function (el) { return el.emailAddress != "opt-out@sustainably.run"; });
        }	
        toRecipients.setAsync(inviteeList,	
          function (asyncResult) {	
            if (asyncResult.status == Office.AsyncResultStatus.Failed) {	
              console.log(asyncResult.error.message);	
            }	
            else {		
              console.log("optout")	
            }	
          }); 	
      }	
    })	

    //opt-out mail id inject ends here

    
  }

  //logout functionality starts
  logoutUser() {
    this.show = false;
    let emailId = Office.context.mailbox.userProfile.emailAddress;
    console.log(emailId);
    this.dashboardSerice.logoutUser(emailId).then(res => {
      this.show = true;
      console.log("logoutUser Success")
    }).catch(err =>{
      console.log("Error occurred at logout");
    })
  }

}
