import { Router } from '@angular/router';
import { OnInit, Component } from '@angular/core';
import axios from 'axios';

@Component({
    selector: "dialob-box",
    templateUrl: "./dialog-box.component.html",
    // styleUrls: ["./dialog-box.component.css"],
  })
  export class DialogBoxComponent implements OnInit {
    userExist: boolean = false;
  
    constructor(private router: Router) {

    }
  
    ngOnInit() {
        axios.get("https://localhost:3030/auth").then(res => {
            console.log(res);
            Office.context.ui.messageParent("done");
        });
    }
      
}