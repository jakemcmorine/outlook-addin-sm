import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, ActivationStart, RouterOutlet } from "@angular/router";
import axios from 'axios';
import { globalConstants } from '../../global';
import { environment } from 'src/environments/environment'

@Component({
  selector: "app-activate-screen",
  templateUrl: "./activate-screen.component.html",
  styleUrls: ["./activate-screen.component.css"],
})
export class ActivateScreenComponent implements OnInit {
  userExist: boolean = false;
  dialog: any;
  signUpUrl: string;
  @ViewChild(RouterOutlet) outlet: RouterOutlet;
  action = "NOT_ACTIVE";
  desktopuser = false;
  cookieMessage = globalConstants.COOKIE_MESSAGE;
  showLoader : boolean = false;

  constructor(private route: ActivatedRoute, private router: Router) {
    if (this.checkCookie()) {
      console.log("Setting Local Storage Property");
      localStorage.setItem("auth", JSON.stringify(this.userExist));
    }
  }

  ngOnInit() {
    this.showLoader = false;
    Office.onReady(() => {
    console.log(Office.context.mailbox.diagnostics.hostName);
    if (window.location.href.indexOf('status=denied') !== -1) {
      console.log("its denying");
      Office.context.ui.messageParent(false);

    }
    if (window.location.href.indexOf('status=success') !== -1) {
      console.log("its Success");
      Office.context.ui.messageParent(true);

    }
    // if(Office.context.mailbox.diagnostics.hostName == 'OutlookWebApp') {
      
      this.desktopuser = this.route.snapshot.params.desktopuser == "desktopuser";
      this.signUpUrl = environment.signUpUrl;
      if (this.checkCookie()) {
        this.action = "ACTIVATE";
        this.router.events.subscribe(e => {
          if (e instanceof ActivationStart && e.snapshot.outlet === "activate")
            this.outlet.deactivate();
        });
      } else {
        this.action = "NOT_ACTIVE";
        this.cookieMessage = globalConstants.COOKIE_MESSAGE;
      }
  
      
    // } else {
    //   this.action = "NOT_ACTIVE";
    //   this.cookieMessage = globalConstants.DESKTOP_USER_MESSAGE;
    // }
  
  })
  }

  checkCookie() {
    let cookieEnabled = navigator.cookieEnabled;
    console.log("Is Cookie Enabled : ", cookieEnabled);
    return cookieEnabled;
  }

  // dialog;
  activateClick() {
    try {
      
      Office.onReady(() => {
        Office.context.ui.displayDialogAsync(
          `${window.location.origin}/auth`,
          {
            height: 60,
            width: 80,
            displayInIframe: false,
          },
          this.processMessage.bind(this)
        );
      });
    } catch (error) {
      console.error("Error setting up dialog");
      console.error(error);
    }
  }

  redirectToNotInstall() {
    this.router.navigate(['/not-install-user']);
  }


  processMessage(asyncResult) {

    this.showLoader = asyncResult.value ? true : false;
    let dialog = asyncResult.value;
    
    dialog.addEventHandler(Office.EventType.DialogEventReceived, (data) => {
      this.showLoader = false;
      console.log("event", data);
      localStorage.setItem('auth', "false");
    });
    dialog.addEventHandler(Office.EventType.DialogMessageReceived, (data) => {
      console.log("message : ", data);
      if (data.message) {
        dialog.close();
        this.userExist = true;
        this.showLoader = false;    
        this.router.navigate(['/dashboard']);
      } else {
        dialog.close();
        this.showLoader = false;
        this.router.navigate(['/activate']);
        localStorage.setItem('auth', JSON.stringify(data.message));

      }
    });

  }
  signUpClk(){
    window.open(this.signUpUrl, "_blank");
  }

}
