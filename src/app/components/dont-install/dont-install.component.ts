import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ActivationStart, RouterOutlet } from "@angular/router";

@Component({
  selector: 'app-dont-install',
  templateUrl: './dont-install.component.html',
  styleUrls: ['./dont-install.component.css']
})
export class DontInstallComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
  }
  backToActivate() {
    this.router.navigate(['../dashboard']);
  }
}
