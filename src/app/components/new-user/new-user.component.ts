import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth-service';
import {globalConstants } from '../../global';
import { environment } from 'src/environments/environment'

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  userType : string;
  userMessage : string;
  signUpUrl : string;

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.userType = this.authService.getUserType();
    switch(this.userType) {
      case globalConstants.ORPHAN_USER :
        this.userMessage = globalConstants.ORPHAN_USER_MESSAGE;      
        this.signUpUrl = environment.signUpUrl;
        console.log("environment.signUpUrl");
        console.log(environment.signUpUrl);
        break;
      case globalConstants.NEW_USER :
        this.userMessage = globalConstants.NEW_USER_MESSAGE;
        break;
    }
  }
}
