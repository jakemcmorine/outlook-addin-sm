import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import * as jquery  from 'jquery';
// import * as office from 'office-js';

// window.Office.onReady(() => {
//   console.log("Office Initialized")
//   jquery.ready.then(() => {
//     console.log("Document.is laoded");
//     platformBrowserDynamic().bootstrapModule(AppModule).then(result => {
//       console.log(result);
//     }).catch(err => console.error(err));
    
//   })
// })



if (environment.production) {
  enableProdMode();
}
console.log("Inside ts file of angular");
declare const Office: any;



Office.onReady(() => {
  console.log("Office Initialized in Dev with AOT in Prod")
  jquery.ready.then(() => {
    console.log("Document.is laoded");
    platformBrowserDynamic().bootstrapModule(AppModule)
      .catch(err => console.error(err));
  })
 
});


Office.initialize = function() {}


