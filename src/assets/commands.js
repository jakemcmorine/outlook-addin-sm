/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

// import { sign } from "crypto";

//  var cheerio = require('cheerio');

// Office.initialize = function () {
    
// };

Office.onReady(function () {
    // If needed, Office.js is ready to be called


/**
 * Shows a notification when the add-in command is executed.
 * @param event {Office.AddinCommands.Event}
 */
function action(event) {

    const message = {
        type: Office.MailboxEnums.ItemNotificationMessageType.InformationalMessage,
        message: "Performed action.",
        icon: "Icon.80x80",
        persistent: true
    }

    // Show a notification message
    Office.context.mailbox.item.notificationMessages.replaceAsync("action", message);

    // Be sure to indicate when the add-in command function is complete
    event.completed({ allowEvent: true });
}



function validateBody(event) {
    console.log("---------", Office.context.mailbox.item.itemType);

    if(Office.MailboxEnums.ItemType.Appointment == Office.context.mailbox.item.itemType && typeof window.localStorage !== 'undefined') {

        let name = Office.context.mailbox.userProfile.emailAddress;
        console.log(name);
        // let Http = new XMLHttpRequest();
        // let url = "https://25805594d974.ngrok.io/getcheck?email="+name;

        // Http.open("GET", url);
        // Http.send();

        let checkBoxValue = localStorage.getItem(Office.context.mailbox.userProfile.emailAddress);

        if (checkBoxValue == "false" && checkBoxValue !== null) {

            console.log("Include Signature");
            Office.context.mailbox.item.body.getAsync('html', function (asyncResult) {
                console.log(asyncResult.value);
                // let signaturePresent = (asyncResult.value.includes('&lt;----')) ? true : false ;
                let signaturePresent = (asyncResult.value.indexOf('&lt;----') > -1) ? true : false;
                console.log("Is signature present : ", signaturePresent);
                let signature = "";
                let smIdentifier = '<p><span style="color:#FFFFFF;"> <---- </span></p>';
                let Http = new XMLHttpRequest();
                let url = "/user/footer/" + name;

                Http.open("GET", url);
                Http.send();

                Http.onreadystatechange = function () {
                    
                    console.log("==========================")

                    if (this.readyState == 4 && this.status == 200) {
                        let returnObject = JSON.parse(Http.response);

                        signaturePresent ? signature = "" : (signature = smIdentifier + returnObject.footer) ;
                        console.log(signature);

                        Office.context.mailbox.item.body.setAsync(
                            asyncResult.value + signature,
                            {
                                coercionType: Office.CoercionType.Html,
                                asyncContext: { var3: 1, var4: 2 }
                            },
                            function (asyncResult1) {
                                if (asyncResult1.status == Office.AsyncResultStatus.Failed) {
                                    console.log("It Failed")
                                    event.completed({ allowEvent: true });
                                }
                                else {
                                    // Successfully set data in item body.
                                    // event.completed();
                                    event.completed({ allowEvent: true });
                                    console.log("Include Signature Success");
                                }
                            }
                        );
                    }
                }

            });
            
        } else {
            console.log("Do not include signature");
            event.completed({ allowEvent: true });
            localStorage.setItem(Office.context.mailbox.userProfile.emailAddress, "true");
        }

    } else {
        event.completed({ allowEvent: true });
        // event.completed();
    }
}

    function getGlobal() {
        return typeof self !== "undefined"
            ? self
            : typeof window !== "undefined"
                ? window
                : typeof global !== "undefined"
                    ? global
                    : undefined;
    }

    const g = getGlobal();

    // the add-in command functions need to be available in global scope
    g.action = action;
    g.validateBody = validateBody;
});




/** Can Be used later - Akshat - do not delete 
 * let signaturePresent = (asyncResult.value.includes('<p id="x_x_identifier">')) ? true : false ;
console.log("Is signature present : ", signaturePresent);
let signature = "";
let smIdentifier = `<p id="identifier"><span style="color:#FFFFFF;"> <---- </span></p>`;
let existingBody = "";
signaturePresent ? ( existingBody = asyncResult.value.slice(0,asyncResult.value.indexOf('<p id="x_x_identifier">')) )  
: existingBody = asyncResult.value ; */
