/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

//  var cheerio = require('cheerio');

Office.onReady(info => {
  // If needed, Office.js is ready to be called
});

/**
 * Shows a notification when the add-in command is executed.
 * @param event {Office.AddinCommands.Event}
 */
function action(event) {

  const message = {
    type: Office.MailboxEnums.ItemNotificationMessageType.InformationalMessage, 
    message: "Performed action.",
    icon: "Icon.80x80",
    persistent: true
  }
  
  // Show a notification message
  Office.context.mailbox.item.notificationMessages.replaceAsync("action", message);

  // Be sure to indicate when the add-in command function is complete
  event.completed();
}



function validateBody(event) {
  console.log("Validating");
  let userEmail = localStorage.getItem(Office.context.mailbox.userProfile.emailAddress);
  let userAuth = localStorage.getItem("auth");

  if(userEmail == null || userEmail == "false") {

    event.completed();
    
  } 
  if(userEmail == 'true' && userAuth == "true") {

    Office.context.mailbox.item.body.getAsync('html' , (asyncResult) => {
      console.log(asyncResult.value);
      let signature = '<p>Email our Team: <a href="mailto:meetings@sustainable-meeting.com">meetings@sustainable-meeting.com</a> </p>'+
      ''+
      '<p>UK Head Office: <a href="tel:00442088956583">0208 895 6583</a></p>'+
      ''+
      '<p><a href="https://meetings.sustainably.run/images/logo.png"><img alt="" src="https://meetings.sustainably.run/assets/img/logo.png" /></a></p>'+
      ''+
      '<p><strong><a href="https://meetings.sustainably.run/by-invitation-only.php">Make your meetings Sustainably Run Meetingss!</a></strong></p>'+
      ''+
      '<p> </p>'+
      ''+
      '<p>Sustainably Run Meetingss Limited  (11889440), The Gatehouse, Kay Street, Summerseat, BL9 5PE</p>'+
      ''+
      '<p>This email and any attachments to it may be confidential and are intended solely for the use of the individual to whom it is addressed. Any views or opinions expressed are solely those of the author and do not necessarily represent those of Sustainably Run Meetingss Limited. If you are not the intended recipient of this email, you must neither take any action based upon its contents nor copy or show it to anyone. Please contact the sender if you believe you have received this email in error. </p>'      
      
      Office.context.mailbox.item.body.setAsync(
        asyncResult.value + signature,
        { coercionType: Office.CoercionType.Html, 
        asyncContext: { var3: 1, var4: 2 } },
        function (asyncResult1) {
            if (asyncResult1.status == 
                Office.AsyncResultStatus.Failed){
                console.log("It Failed")
            }
            else {
              // Successfully set data in item body.
              event.completed();
            }
        });
      
    });
  }

  
  // Office.context.mailbox.item.body.a
}

function getGlobal() {
  return typeof self !== "undefined"
    ? self
    : typeof window !== "undefined"
    ? window
    : typeof global !== "undefined"
    ? global
    : undefined;
}

const g = getGlobal();

// the add-in command functions need to be available in global scope
g.action = action;
g.validateBody = validateBody;
