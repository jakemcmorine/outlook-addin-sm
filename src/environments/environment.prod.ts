export const environment = {
  production: true,
  pluginLoginApi: "https://meetings.sustainably.run/pluginLogin?token=",
  signUpUrl: "https://meetings.sustainably.run/signup"
};
