const recurringMeetingRepository = require('../respository/recurringMeetingsRepository');
const timeZoneDetailsRepo = require('../respository/timeZoneDetailsRepository');
const meetingRepository = require('../respository/MeetingRepository');
var meetingController = require('../controllers/MeetingController');
const WEEK_DAYS = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
const util = require('util')

let currentDate = new Date();
let currentDateWithOffset;
let meetingStartDateWithOffset;
let recurringMeetingDetail = {};

/**
 * Following method is called to create or upate an RM
 * Based on changeType ('created' / 'updated' ) field received in parameter. We perform the action
 */
  module.exports.saveRecurringMeetingData = async (calendarResource, changeType, message, userId, graph) => {
    recurringMeetingDetail = {};
    console.log(calendarResource, message);
    console.log('----Change type ----------', changeType, " For user id : " , userId);
    let meetingEndTIme = calendarResource['end']['dateTime'].slice(10);
    let calendarResourceRecurrence = calendarResource['recurrence'];
    // let currentDate = new Date();

    let startDate = new Date();
    startDate.setHours(00);
    startDate.setMinutes(01);

    let endDate = new Date();
    endDate.setHours(23);
    endDate.setMinutes(59);

    let instanceUrl = message.resource + `/instances?startDateTime=${startDate.toISOString()}&endDateTime=${endDate.toISOString()}&$top=1000`;
    // console.log(instanceUrl);
    /** Following graph api call gets all instances (one instance for today ) for particular resource.*/
    let instancesReceived = await graph.api(instanceUrl).get();
    let instances = instancesReceived.value;
    console.log(`----Today's Instance ---- `, instances[0]);
    /**
     * Preparing the object to be saved in RM table ( whether on creation or updation )
     */
    recurringMeetingDetail['calendar_type'] = 'outlookWeb';
    recurringMeetingDetail['subject'] = calendarResource['subject'];
    recurringMeetingDetail['is_all_day'] = calendarResource['isAllDay'] ? 'Y' : 'N';
    recurringMeetingDetail['start_date'] = new Date(calendarResource['start']['dateTime']);
    recurringMeetingDetail['end_date'] = new Date(calendarResourceRecurrence['range']['endDate'] + meetingEndTIme);
    recurringMeetingDetail['repeat_every'] = calendarResourceRecurrence['pattern']['interval'];
    // if (calendarResourceRecurrence['pattern']['type'].includes("Yearly")) {
      if (calendarResourceRecurrence['range']['endDate'] === "0001-01-01") {
        recurringMeetingDetail['end_date'].setFullYear(recurringMeetingDetail.start_date.getFullYear() + 10);
        recurringMeetingDetail['end_date'].setMonth(recurringMeetingDetail.start_date.getMonth());
        recurringMeetingDetail['end_date'].setDate(recurringMeetingDetail.start_date.getDate() + 2);
      }
    // }
    recurringMeetingDetail['frequency_type'] = calendarResourceRecurrence['pattern']['type'];
    recurringMeetingDetail['day_index'] = calendarResourceRecurrence['pattern']['index'];
    if (calendarResourceRecurrence['pattern']['daysOfWeek'] !== undefined)
      recurringMeetingDetail['weekdays_selected'] = calendarResourceRecurrence['pattern']['daysOfWeek'].toString();
    else
       recurringMeetingDetail['weekdays_selected'] = null;
    recurringMeetingDetail['organizer'] = calendarResource['organizer']['emailAddress'].address;
    recurringMeetingDetail['TimeZoneName'] = calendarResource['originalStartTimeZone'];
    recurringMeetingDetail['TimeZoneOffset'] = await getTimeZoneOffsetValue(calendarResource['originalStartTimeZone']);
    recurringMeetingDetail['countable'] = 1;
  

    //opt-out functionality starts here
    let incomingInviteeDetailed = [];
      let meetingInvites = [];
      for (let j = 0; j < calendarResource.attendees.length; j++) {
        if ((calendarResource.attendees[j].status.response != "declined") && (calendarResource.attendees[j].emailAddress.address != calendarResource.organizer.emailAddress.address)) {
          meetingInvites.push(calendarResource.attendees[j].emailAddress.address)
          incomingInviteeDetailed.push({ firstname: calendarResource.attendees[j].emailAddress.name, email: calendarResource.attendees[j].emailAddress.address });
        }
      }

    let optOutVal = [];
    if (incomingInviteeDetailed) {
        optOutVal = incomingInviteeDetailed.filter(function (listItem) {
            return listItem.email == 'opt-out@sustainably.run';
        });
    }
    
    if(calendarResource['bodyPreview'].includes("<---")) {
      recurringMeetingDetail['countable'] = 1;
    }
    if(optOutVal.length>0) {
      recurringMeetingDetail['countable'] = 0;
    }

    //opt-out functionality ends here

    let rmDetails;
    if (changeType === "created") {
      // console.log("------------------------Creating record--------------------------", util.inspect(calendarResource, false, null, true));
      rmDetails = await saveRecurringMeeting(calendarResource, message, instances,userId);
    } else {
      // console.log("----------Testing for this and following : -----------", recurringMeetingDetail);
      let existingRm = await recurringMeetingRepository.getRecurringMeeting(calendarResource.iCalUId);
      
      if(existingRm !== null && existingRm['cancelled_at'] == null) {
        
        console.log(`-----------Updating record for : ${existingRm.recurring_meeting_id}-----------`);

        let isDateChanged = await checkIsDateChanged(existingRm, recurringMeetingDetail);
        console.log("Is Date Changed : ", isDateChanged);
        /**
         * Following If consition is executed when any of these RM fields are changed : 
         * start_date, end_date , frequency , day_index , interval ( repeat_every), weekdays_selected
         * It sets the next meetingDate to be saved in RM Table (To be used by cron job)
         * It then cancels the existing rm instances related to this RM
         * It updates the RM table with new dates 
         * Checks if start date of meeting is today , then it will create a meeting 
         */
        if(isDateChanged){
          let meetingStartDateWithOffset;
          let isCertTriggerredForExistingMeeting = false; //Flag to check if cert is already triggered for aparticular meeting
                 /**
           * If next meeting is today , then set meetingStartDateWithOffset with nextMeeting date 
           * otherwise with calendar startDate
           */
          if(instances.length !== 0) {

            let instanceStartDate = new Date(instances[0]['start']['dateTime']);
            meetingStartDateWithOffset = calculateDateTimeWithOffset(instanceStartDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
            
            if(meetingStartDateWithOffset.toDateString() == currentDate.toDateString()) {

            calendarResource.start.dateTime = new String(instanceStartDate.toISOString());
            /** So next meeting date will be set considering today's instacne start date */
            recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(calendarResource, recurringMeetingDetail['TimeZoneOffset']);
    
            }
            
            /********************************************************************************************************
             * ********************START OF HANDLING CERT TRIGGER AND MEETING UPDATE******************************** */
            isCertTriggerredForExistingMeeting = await meetingRepository.checkIfCertTrigForMeeting(instances[0].id);
            console.log(`------Is Certificate Triggrred for today's meeting : -------`, isCertTriggerredForExistingMeeting);
            
            if(isCertTriggerredForExistingMeeting) {
              console.log(`-----------Updating today's Instance -----------`);
              instances[0]['rm_id'] = calendarResource.id;
              instances[0]['iCalUId'] =calendarResource.iCalUId;
              meetingController.updateMeeting(instances[0], userId);
            } else {
              console.log(`-----------Cancelling today's Instance `);
            }
            /********************************************************************************************************
             * **************************END OF HANDLING CERT TRIGGER AND MEETING UPDATE***************************** */

          } else {

            recurringMeetingDetail['next_meeting_date'] = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
            while (recurringMeetingDetail.next_meeting_date < currentDate) {
              calendarResource.start.dateTime = calculateDateTimeWithoutOffset(recurringMeetingDetail.next_meeting_date, recurringMeetingDetail['TimeZoneOffset']);
              recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(calendarResource, recurringMeetingDetail['TimeZoneOffset']);
            }
            meetingStartDateWithOffset = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
            
            // recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(calendarResource, recurringMeetingDetail['TimeZoneOffset']);
            // meetingStartDateWithOffset = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime() , recurringMeetingDetail['TimeZoneOffset']);
          }
  
          // recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(calendarResource, recurringMeetingDetail['TimeZoneOffset']);
          if(!isCertTriggerredForExistingMeeting) {
            console.log('-----Cert not sent thus deleting the meeting and stopping the step function executing')
            let stateMachineArn = await meetingRepository.cancelRmInstances(calendarResource.id);
            if(stateMachineArn !== undefined) {
              for(let i=0;i<stateMachineArn.length ; i ++) {
                await meetingController.stopStepFunction(stateMachineArn[i])
              }
            }
          }
          
     
          rmDetails = await recurringMeetingRepository.updateRecurringMeetings(recurringMeetingDetail, existingRm.outlook_meeting_id);
          
          // let meetingStartDateWithOffset = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
          let currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
          console.log("Current Date With Offset : ", currentDateWithOffset, " Meeting Start Date With Offset : ", meetingStartDateWithOffset);
          
          if(!isCertTriggerredForExistingMeeting && (currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime()) && instances.length !== 0 ||
          (instances[0] && instances[0].isAllDay && (currentDate.toDateString() == meetingStartDateWithOffset.toDateString())) ) {
            //Create a meeting
            console.log("-----------Creating updated meeting -------------");
            if(instances.length !== 0) {
              instances[0]['rm_id'] = calendarResource.id;
              instances[0]['iCalUId'] =calendarResource.iCalUId;
              meetingController.createMeeting(instances[0], userId);
            }
          }
          
  
        } else {

          
          rmDetails = await recurringMeetingRepository.updateRecurringMeetings(recurringMeetingDetail, calendarResource.iCalUId);
          
          if(rmDetails !== null && rmDetails !== undefined) {
            
            /**
             * Following piece of code gets the existing rm instances in meetings table with conditions : 
             * START_DATE >= Today' date with time : TODAY'S_DAY-00:01:00 AND rm_id = calendarResource.id
             */
            let startDate = new Date();
            // startDate.setHours(00);
            // startDate.setMinutes(01);
            let scheduledRmInstances = await meetingRepository.getExistingInstances(rmDetails.resource_id, startDate.toISOString());
  
            if(scheduledRmInstances.length !== 0) {
              
              console.log(`-------- Updated today's meeting ---------`);
              for(let i =0; i< instances.length ; i++ ){
                
                /** Following code checks of the instance ( meeting to occur today ) is found in scheduledRMInstances
                 * Then it will update that instance in meeting_table also its corresponsing meeting_invitees table.
                 */
                // console.log("---------- Instacne Received attendees : ----------", instances[i]['attendees']);
                // console.log("---------Update meeting table for this instance ---------");

                instances[i]['iCalUId'] = rmDetails.outlook_meeting_id;
                await meetingController.updateMeeting(instances[i]);
    
              }
              //If today's meeting is deleted , then cancel that rm instance and delete the step function
              if(instances.length == 0) {
                for(let i =0 ;i < scheduledRmInstances.length ; i++ ) {
                  let stateMachineArn = await meetingRepository.cancelRmInstances(scheduledRmInstances[i].rm_id);
                  if(stateMachineArn !== undefined) {
                    for(let i=0;i<stateMachineArn.length ; i ++) {
                      await meetingController.stopStepFunction(stateMachineArn[i])
                    }
                  }
                }
              }
            }
            else {
  
                if(instances.length !== 0 && rmDetails.cancelled_at == null ) {
                  
                  console.log("---------------Creating Today's Meeting ---------------");
  
                  let todayMeetingDate = new Date(instances[0]['start']['dateTime']);
  
                  let meetingStartDateWithOffset = calculateDateTimeWithOffset(todayMeetingDate.getTime(), recurringMeetingDetail['TimeZoneOffset']);
                  let currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
                  
                  if(currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime()) {
                    instances[0]['rm_id'] = rmDetails.resource_id
                    instances[0]['iCalUId'] = rmDetails.outlook_meeting_id;
                    meetingController.createMeeting(instances[0], userId);   
                  }
                } else {
                  console.log(`-------- Possibility that it either future or past instance is updated --------`);
                }
          }
        }
        }
      } else if(existingRm == null) {

        console.log(`-------Creating new meeting and RM-------`);  
        //For the iCalUid check if the meeting is there . if yes check if the cert is triggered . If yes , just update , 
        //if no : cancel that meeting and create a new meeting and rm
        let isCertTriggerredForExistingMeeting = await meetingRepository.getMeetingByIcalUid(calendarResource.iCalUId);
        console.log(`------Is Certificate Triggrred for today's meeting : -------`, isCertTriggerredForExistingMeeting);
        
        if(isCertTriggerredForExistingMeeting.isSent) {
          console.log(`-----------Updating today's Instance -----------`);
          if(instances.length) {
            instances[0]['rm_id'] = calendarResource.id;
            instances[0]['iCalUId'] =calendarResource.iCalUId;
            meetingController.updateMeeting(instances[0], userId);
          }
        } else {
          console.log(`-----------Cancelling today's Instance and creating new ----------- `);
          if(isCertTriggerredForExistingMeeting.meetingId)
            await meetingController.cancelMeeting(isCertTriggerredForExistingMeeting.meetingId);
        }
        let rmDetails = await saveRecurringMeeting(calendarResource, message, instances, userId);

      }
   
    }
  }

  /**
   * 
   * @param {*} recurringMeetingDetail 
   * @param {*} calendarResource 
   * @param {*} message 
   * @param {*} instances 
   * @param {*} currentDate 
   */
  async function saveRecurringMeeting(calendarResource, message, instances,userId) {

      recurringMeetingDetail['resource'] = `Users/${calendarResource.organizer.emailAddress.address}/Events/${calendarResource.id}`;  
      
      // recurringMeetingDetail['resource'] = message.resource;
      console.log('----While Saving RM meesage resource : ', message.resource);
      console.log('----Saving RM with resource : ', recurringMeetingDetail['resource']);
      
      recurringMeetingDetail['resource_id'] = calendarResource['id'];
      recurringMeetingDetail['outlook_meeting_id'] = calendarResource.iCalUId;
      // recurringMeetingDetail['subscription_id'] = message.subscriptionId;
      let rmDetails;
      currentDateWithOffset = calculateDateTimeWithOffset(currentDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
      /**
       * If next meeting is today , then set meetingStartDateWithOffset with nextMeeting date 
       * otherwise with calendar startDate
       */
        if(instances.length !== 0) {

         let instanceStartDate = new Date(instances[0]['start']['dateTime']);
         meetingStartDateWithOffset = calculateDateTimeWithOffset(instanceStartDate.getTime(), recurringMeetingDetail.TimeZoneOffset);
         
         if(meetingStartDateWithOffset.toDateString() == currentDateWithOffset.toDateString()) {

          calendarResource.start.dateTime = new String(instanceStartDate.toISOString());
          /** So next meeting date will be set considering today's instacne start date */
          recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(calendarResource, recurringMeetingDetail['TimeZoneOffset']);
  
         }
       } else {
          // recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(calendarResource, recurringMeetingDetail['TimeZoneOffset']);
          recurringMeetingDetail['next_meeting_date'] = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime(), recurringMeetingDetail['TimeZoneOffset']);
          
          while(recurringMeetingDetail.next_meeting_date < currentDate) {
            calendarResource.start.dateTime = calculateDateTimeWithoutOffset(recurringMeetingDetail.next_meeting_date, recurringMeetingDetail['TimeZoneOffset']);
            recurringMeetingDetail['next_meeting_date'] = nextsetNextMeetingDate(calendarResource, recurringMeetingDetail['TimeZoneOffset']);
          }
          meetingStartDateWithOffset = calculateDateTimeWithOffset(recurringMeetingDetail.start_date.getTime() , recurringMeetingDetail['TimeZoneOffset']);
       }
       console.log("===> Next Meeting Date : ", recurringMeetingDetail['next_meeting_date']);
      let rmData = await recurringMeetingRepository.getRecurringMeeting(calendarResource.iCalUId);
      if (rmData !== null){
        //update RM
        let updatedData = await recurringMeetingRepository.updateRecurringMeetings(recurringMeetingDetail, calendarResource.iCalUId);
        // console.log("------updatedData");
        // console.log(updatedData);
      }else{
        rmDetails = await recurringMeetingRepository.saveRecurringMeetings(recurringMeetingDetail);
        // return id; //returning recurring_meeting_id
      }

            
      if(rmDetails !== null) {
        // new Date(calendarResource['start']['dateTime']);
        
        console.log("Current Date With Offset : ", currentDateWithOffset, " Meeting Start Date With Offset : ", meetingStartDateWithOffset);
        /**
         * Following condition checks if the start date of RM is today . Then create a meeting instance
         * in meetings table and corresponding meeting_invitees table.
         * Provide the rm_id for later reference ( updating , deleting )
         */
        //Following or-if condition is to handle creation of all meeting instance in meetings table.
        if( (currentDateWithOffset.getTime() <= meetingStartDateWithOffset.getTime()) || 
            (recurringMeetingDetail.is_all_day === 'Y' && (currentDate.toDateString() == meetingStartDateWithOffset.toDateString())) ) {
          //Create a meeting
          if(instances.length !== 0) {
            console.log("-----------------Creating today's Instace --------------------", instances[0]['start']);
            instances[0]['rm_id'] = calendarResource.id;
            instances[0]['iCalUId'] =calendarResource.iCalUId;
            meetingController.createMeeting(instances[0], userId);
          }
        }
        
      }
      
  }
   
  /**
   * Following method sends a boolean value whether any field related to dates, frequency is changed 
   * Those fields which play a role in deciding when is the next meeting date going to occur.
   * @param {*} existingRmData 
   * @param {*} incomingRmData 
   */
  function checkIsDateChanged(existingRmData, incomingRmData) {
    if(existingRmData !== null && incomingRmData !== null ) {

        if(existingRmData.start_date.getTime() !== incomingRmData.start_date.getTime()) {
          console.log(`----Start Date is changed to : ${incomingRmData.start_date} from : ${existingRmData.start_date}---`);
          return true;
        }
        else if(existingRmData.end_date.getTime() !== incomingRmData.end_date.getTime()) {
          console.log(`----End Date is changed to : ${incomingRmData.end_date} from : ${existingRmData.end_date}---`);
          return true;
        }
        else if(existingRmData.repeat_every !== incomingRmData.repeat_every) {
          console.log(`----Interval is changed to : ${incomingRmData.repeat_every} from : ${existingRmData.repeat_every} ----`);
          return true;
        }
        else if(existingRmData.frequency_type !== incomingRmData.frequency_type) {
          console.log(`---Frequency Type is changed to ${incomingRmData.frequency_type} from ${existingRmData.frequency_type} ---`);
          return true;
        }
        else if(existingRmData.day_index !== incomingRmData.day_index) {
          console.log(`--- Day Index changed to ${incomingRmData.day_index} from ${existingRmData.day_index} ----`);
          return true;
        }
        else if(existingRmData.weekdays_selected !== incomingRmData.weekdays_selected ) {
          console.log(`--- Weekdays Selected changed to ${incomingRmData.weekdays_selected} from ${existingRmData.weekdays_selected} ---`);
          return true
        } else return false;

    } else return false;
  }
  
  /**
   * Following method accept these two params and provides an output of time with offset added
   * @param {*} nextMeetingDateTime 
   * @param {*} timeZoneOffset 
   */
  function calculateDateTimeWithOffset(nextMeetingDateTime, timeZoneOffset) {
    
    let totalMinutes = 0;
    let totalSeconds = 0;
    let sign = timeZoneOffset.slice(0, 1); //This can be either + or -
    let timeToAddSubtract = new String(timeZoneOffset.slice(1));

  
    if (timeToAddSubtract.includes(":") || timeToAddSubtract.includes(".")) {
      let hour = parseInt(timeToAddSubtract.slice(0, timeToAddSubtract.indexOf(":")));
      let minutes = parseInt(timeToAddSubtract.slice(timeToAddSubtract.indexOf(":") + 1));

      totalSeconds = ((hour * 60) + minutes) * 60; //Converting to seconds
    } else {
      totalSeconds = timeToAddSubtract * 60 * 60; //Converting Hours to seconds
    }
    if (sign == "?") {
      return new Date(nextMeetingDateTime - (totalSeconds * 1000));
    } else {
      return new Date(nextMeetingDateTime + (totalSeconds * 1000));
    }
  
  }

  /**
   * 
   * @param {*} dateTime 
   * @param {*} timeZoneOffset 
   */
  function calculateDateTimeWithoutOffset(dateTime, timeZoneOffset) {
    let sign = timeZoneOffset.slice(0,1); //This can be either + or -
    let totalSeconds = getTimeZoneOffsetSeconds(timeZoneOffset);
    if(sign == "?") {
        return new Date(dateTime + (totalSeconds * 1000));
    } else {
        return new Date(dateTime - (totalSeconds * 1000));
    }
}
  /**
   * 
   * @param {*} timeZoneOffset 
   */
  function getTimeZoneOffsetSeconds(timeZoneOffset) {
      
    let timeToAddSubtract = new String(timeZoneOffset.slice(1));
    if(timeToAddSubtract.includes(":") || timeToAddSubtract.includes(".")) {
        let hour = parseInt(timeToAddSubtract.slice(0, timeToAddSubtract.indexOf(":")));
        let minutes = parseInt(timeToAddSubtract.slice(timeToAddSubtract.indexOf(":")+1));
        return (( hour*60 ) + minutes) * 60; //Converting to seconds
    } else {
        return totalSeconds = timeToAddSubtract * 60 * 60 ; //Converting Hours to seconds
    }
  }
  
  /**
   * Following method calculates the next meeting date for that rm .
   * There are 6 cases : WEEKLY , DAILY , ABSOLUTE MONTHLY , RELATIVE MONTHLY , ABSOLUTE YEARLY AND RELATIVE YEARLY
   * Based on any of above frequency_type , we calculate the next meeting date with the offset added to it.
   * @param {*} calendarResource 
   * @param {*} timeZoneOffset 
   */
  function nextsetNextMeetingDate(calendarResource, timeZoneOffset) {
    let interval = calendarResource.recurrence.pattern.interval;
    let startDay = calendarResource.start.dateTime;
    let nextMeetingDate = new Date(startDay);
    let index = calendarResource.recurrence.pattern.index;
    
    if (calendarResource.recurrence.pattern.type == "weekly") {
  
      let weekdaysSelected = calendarResource.recurrence['pattern']['daysOfWeek'];
      
      if(weekdaysSelected.length == 1) {
                            
        nextMeetingDate.setDate(nextMeetingDate.getDate() + 7 * interval);
        return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);

      } else {
        
        let meetingDay = WEEK_DAYS[nextMeetingDate.getDay()];
        // console.log(`--------meeting day : ${meetingDay}-----------------`);


        if(weekdaysSelected.indexOf(meetingDay) == (weekdaysSelected.length - 1) ) {
            //Set next meeting date after weeks from 1st day in array.
            let gapBwFirstnLastMeetingInWeek = WEEK_DAYS.indexOf(meetingDay) - WEEK_DAYS.indexOf(weekdaysSelected[0])
            let repeatMeetingAfter = (interval - 1 ) * 7 + (7 - gapBwFirstnLastMeetingInWeek);
            
            nextMeetingDate.setDate(nextMeetingDate.getDate() + repeatMeetingAfter);
            return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);

        } else {
            //Set next meeting day for next day in weekDaysSelected array.
            // console.log("------Weekdays selected are : ------",weekdaysSelected );
            let meetingDayIndex = weekdaysSelected.indexOf(meetingDay); //This will give index of day in weekDaysSelected array in DB
            
            //Following code will give the week day number (b)
            let meetingWeekDay = WEEK_DAYS.indexOf(meetingDay);
            let nextMeetingWeekDay = WEEK_DAYS.indexOf(weekdaysSelected[meetingDayIndex + 1]); 
            
            let repeatMeetingAfter;
            ( nextMeetingWeekDay < meetingWeekDay ) ? 
            ( repeatMeetingAfter = 7 - meetingWeekDay + nextMeetingWeekDay ) : ( repeatMeetingAfter = nextMeetingWeekDay - meetingWeekDay);
            
            // console.log(`--------nextMeetingWeekDay : ${nextMeetingWeekDay} repeatMeetingAfter : ${repeatMeetingAfter} -----------------`);
            
            nextMeetingDate.setDate(nextMeetingDate.getDate() + repeatMeetingAfter);
            return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);
        }
    }

  
    } else if (calendarResource.recurrence.pattern.type == "absoluteMonthly") {
  
      nextMeetingDate.setMonth(nextMeetingDate.getMonth() + interval);
      return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);
  
    } else if (calendarResource.recurrence.pattern.type == "relativeMonthly") {
  
      let dayIndex = nextMeetingDate.getDay();
      let monthIndex = nextMeetingDate.getMonth();
      let nextMonthIndex = monthIndex + interval;
  
      let firstDayOfNextMonth = new Date(nextMeetingDate.getFullYear(), nextMonthIndex, 1);
      firstDayOfNextMonth.setHours(nextMeetingDate.getHours());
      firstDayOfNextMonth.setMinutes(nextMeetingDate.getMinutes());
      // console.log("FIrst day of next month ", firstDayOfNextMonth);
  
      if (index == "first") {
        if (dayIndex == firstDayOfNextMonth.getDay()) {
          // return firstDayOfNextMonth;
          return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime() + timeZoneOffset);
        } else {
          while (firstDayOfNextMonth.getDay() !== dayIndex) {
            firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
          }
          // return firstDayOfNextMonth;
          return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset);
        }
      }
      if (index == "second") {
        while (firstDayOfNextMonth.getDay() !== dayIndex) {
          firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7)
        return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset);
      }
      if (index == "third") {
        while (firstDayOfNextMonth.getDay() !== dayIndex) {
          firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7 * 2);
        return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset);
      }
      if (index == "fourth") {
        while (firstDayOfNextMonth.getDay() !== dayIndex) {
          firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 1);
        }
        firstDayOfNextMonth.setDate(firstDayOfNextMonth.getDate() + 7 * 3);
        return calculateDateTimeWithOffset(firstDayOfNextMonth.getTime(), timeZoneOffset);
      }
      if (index == "last") {
        let lastDayOfNextMonth = new Date(firstDayOfNextMonth.getFullYear(), firstDayOfNextMonth.getMonth() + 1, 0, firstDayOfNextMonth.getHours(), firstDayOfNextMonth.getMinutes(), firstDayOfNextMonth.getSeconds());
        // console.log("Last Day of Next Month is : ", lastDayOfNextMonth);
        while (lastDayOfNextMonth.getDay() !== dayIndex) {
          lastDayOfNextMonth.setDate(lastDayOfNextMonth.getDate() - 1);
        }
        // return lastDayOfNextMonth;
        return calculateDateTimeWithOffset(lastDayOfNextMonth.getTime(), timeZoneOffset);
      }
  
    } else if (calendarResource.recurrence.pattern.type == "daily") {
      nextMeetingDate.setDate(nextMeetingDate.getDate() + interval)
      return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);
  
    } else if (calendarResource.recurrence.pattern.type == "relativeYearly") {
  
      let dayIndex = nextMeetingDate.getDay();
  
      let firsDayOfSameMonthNextYear = new Date(nextMeetingDate.getFullYear() + 1, nextMeetingDate.getMonth(), 1);
      firsDayOfSameMonthNextYear.setHours(nextMeetingDate.getHours());
      firsDayOfSameMonthNextYear.setMinutes(nextMeetingDate.getMinutes());
      firsDayOfSameMonthNextYear.setSeconds(nextMeetingDate.getSeconds());
  
      if (index == "first") {
        if (dayIndex == firsDayOfSameMonthNextYear.getDay()) {
          // return firsDayOfSameMonthNextYear;
          return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
        } else {
          while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
            firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
          }
          // return firsDayOfSameMonthNextYear;
          return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
        }
      }
      if (index == "second") {
        while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
          firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7);
        return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
      }
      if (index == "third") {
        while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
          firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7 * 2);
        return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
      }
      if (index == "fourth") {
        while (firsDayOfSameMonthNextYear.getDay() !== dayIndex) {
          firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 1);
        }
        firsDayOfSameMonthNextYear.setDate(firsDayOfSameMonthNextYear.getDate() + 7 * 3);
        return calculateDateTimeWithOffset(firsDayOfSameMonthNextYear.getTime(), timeZoneOffset);
      }
      if (index == "last") {
        let lastDayOfSameMonthNextYear = new Date(firsDayOfSameMonthNextYear.getFullYear(), firsDayOfSameMonthNextYear.getMonth() + 1, 0, firsDayOfSameMonthNextYear.getHours(), firsDayOfSameMonthNextYear.getMinutes(), firsDayOfSameMonthNextYear.getSeconds());
        // console.log("Last Day of sam Month next year is : ", lastDayOfSameMonthNextYear);
        while (lastDayOfSameMonthNextYear.getDay() !== dayIndex) {
          lastDayOfSameMonthNextYear.setDate(lastDayOfSameMonthNextYear.getDate() - 1);
        }
        lastDayOfSameMonthNextYear;
        return calculateDateTimeWithOffset(lastDayOfSameMonthNextYear.getTime(), TimeZoneOffset);
      }
  
    } else if (calendarResource.recurrence.pattern.type == "absoluteYearly") {
      nextMeetingDate.setFullYear(nextMeetingDate.getFullYear() + interval);
      return calculateDateTimeWithOffset(nextMeetingDate.getTime(), timeZoneOffset);
    }
  }
  
  /**
   * Following method will be called to get the timezone offset value from time_zone_Details table.
   * It accepts the timezone name eg : India Standard TIme and provide offset wrt to URC
   * @param {*} timeZoneName 
   */
  async function getTimeZoneOffsetValue(timeZoneName) {
    // console.log("-------------Time Zone Name is : -----------------", timeZoneName);
    let timeZoneOffset = await timeZoneDetailsRepo.getTimeZoneOffsetValue(timeZoneName);
    return timeZoneOffset;
  }

  /**
   * Following method accepts resourceId ( message.resourceData.id ) and helps us identify whethere 
   * this resource is an RM or not. 
   */
  module.exports.checkIfRm = async (resourceId) => {

    let isRm = await recurringMeetingRepository.checkIfRm(resourceId);
    console.log(isRm);
    return isRm;

  }

  /**
   * Following function accepts resource_id ( rmId ) and cancels the recurring meeting 
   * by setting the cancellattion date and furthere deleting rm instances in meeting table related to it
   * Also in the end deleted meeting invitees for those instances deleted
   */
  module.exports.cancelRecurringMeeting = async (rmId) => {
    console.log("--------------------------Cancelling Recurring Meeting --------------------------");

    let condition = { where: { 'resource_id': rmId } };
    let removeData = await recurringMeetingRepository.cancelRecurringMeeting(condition);
    let stateMachineArn = await meetingRepository.cancelRmInstances(rmId);
    if(stateMachineArn !== undefined && stateMachineArn.length !== 0) {
      for(let i=0;i<stateMachineArn.length ; i ++) {
        await meetingController.stopStepFunction(stateMachineArn[i])
      }
    }
    
    return true;

  }


 