const axios = require('axios');
var userRepository = require('../respository/UserRepository');
const { Client } = require('@microsoft/microsoft-graph-client');
const userTokenModel = require("../models").user_token;
const pluginStatusModel = require("../models").plugin_status;
/**
 * Following method gives user details based on email id.
 */
module.exports.userDetailsByEmailId = (req,res) => {

    console.log(req.params.emailId);
    // mysql.query("select * from users where `email` = ? ",[req.params.userId] , (err, results) => {
    //     res.send(results);
    // });
    let emailId = req.params.emailId;

    userRepository.userDetailsByEmailId(emailId).then(result => {

        console.log("Successfully got user details ");
        res.status(200).send(result);

    }).catch(error => {

        console.log("Error occurred while getting user details : " , error);
        res.status(500).json({Message : "Error Occurred while getting user details"});

    });
};

module.exports.getToken = (req,res) => {
    console.log(req.body);
    let data = req.body;
    if(data){
        let finalUrl = process.env.AUTH_BASE_URL+`/v1/directLogin/` + data.userId;
        return axios.post(finalUrl,{}).then(response => {
        //   return response.data;
            return res.status(200).json(response.data);
        })
        .catch(error => {
          console.log("Error Occurred while getting LoginToken");
        })
    }
    res.status(200).json({ Message: "Error Occurred while getting login token" });
};

module.exports.logout = async (req, res) => {
    try {
        console.log(req.body);
        let result = await userTokenModel.findOne({ where: { email: req.body.email } });
        // console.log(result);
        try {
            let accessToken = result.accessToken;
            const graph = Client.initWithMiddleware({
                authProvider: {
                    getAccessToken: async () => accessToken
                }
            });
            let webhookReceived = await graph.api('/subscriptions').get();
            console.log(webhookReceived);
            if (webhookReceived.value.length !== 0) {
                for (let i = 0; i < webhookReceived.value.length; i++) {
                    console.log(`Deleting ${webhookReceived.value[i].id} subscription`);
                    let deletedHook = await graph.api(`/subscriptions/${webhookReceived.value[i].id}`).delete();
                    console.log(deletedHook);
                }   
            }
        } catch (error) {
            console.log("Graph Error at logout");
        }
        let result1 = await userTokenModel.destroy({ where: { email: req.body.email } });
        console.log(result1);
        let result2 = await pluginStatusModel.changePluginStatus(0, req.body.email,"outlookDesktop");
        console.log(result2);
        return res.status(200).json({Message: "Successfully Logout"});
    } catch (error) {
        console.log(error);
        return res.status(300).json({ Message: "Some error occurred" });
    }
};

