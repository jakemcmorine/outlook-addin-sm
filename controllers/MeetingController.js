// const mysql = require('../config/db-config');
var meetingRepository = require('../respository/MeetingRepository');
var userRepository = require('../respository/UserRepository');
var timeZoneDetailsRepo = require('../respository/timeZoneDetailsRepository');
var treeCountRepo = require('../respository/treeCountRepository');

let AWS_ACCESSKEY = "4UxIUeeut6KrdjiXURblVfRl1SviJMUuWjw1/hxx";
// let AWS_ACCESSKEY = "qphnTv/apz5RcIeony5oAvwIskre/DDhHhvdgv9h";
let AWS_ACCESSKEYID = "AKIAYLWKX2YUDFXGUG5P";
// let AWS_ACCESSKEYID = "AKIATXAUUPSJF2SPQEQN";
let AWS_REGION = "eu-west-1";

const AWS = require('aws-sdk');
AWS.config.update({
  region: AWS_REGION,
  accessKeyId: AWS_ACCESSKEYID,
  secretAccessKey: AWS_ACCESSKEY
})
const stepfunctions = new AWS.StepFunctions();

/**
 * The following method gives meeting count of a particular user based on user_id.
 * THis gives the number of meetings attended and number of tress planted based on countable field ( 1 or 0)
 */
module.exports.meetingCountOfUser = async (req, res) => {

  
  let userId = req.body.userId;
  let initialTreeCount = (req.body.treeNumber !== null) ? req.body.treeNumber : 0;

  console.log(`---Calling Meeting Count for a User : ${userId}---`);

  //Check whether the user is an IP or not.
  let isUserIp = false;
  if(userId !== undefined) {
    isUserIp = await meetingRepository.checkUserIp(userId);
  } 

  if(isUserIp) {

    if(userId !== null && userId !== "" && userId !== undefined) {
      console.log(`------------------Calling meeting count for an IP : ${userId}-----------------------`);
      
      const meetings = await meetingRepository.meetingCount({ org_id: userId, countable : 1 });
      const meetingInvite = await treeCountRepo.getTreeCount(userId, false);
      res.json({ meetingCount: meetings, meetingInviteCount: meetingInvite[0].treeCount})
    } else {
      console.log("Invalid user id : ", userId);
      res.json({ meetingCount: 0, meetingInviteCount: 0 })
    }

  } else {
    
    console.log("---User is not an IP and id is : ", userId);
    if(userId !== null && userId !== "" && userId !== undefined) {
    try {
      const mettings = await meetingRepository.meetingCount({ user_id: userId, countable: 1 });
      const meetingInvite = await treeCountRepo.getTreeCount(userId, true);
      console.log(meetingInvite);
      console.log("-------------meetings = ", mettings, "---------------------meetingInvite ---------", meetingInvite[0].treeCount);
      res.json({ meetingCount: mettings, meetingInviteCount: meetingInvite[0].treeCount })
  
    } catch(error) {
      console.log("Error Occurred while getting meetings count : ", error);
      res.json({ meetingCount: 0, meetingInviteCount: 0 })
    }
    
    } else {
      console.log("Invalid user id : ", userId);
      res.json({ meetingCount: 0, meetingInviteCount: 0 })
    }

  }



};

module.exports.getFooter = async (req, res) => {
  let email = req.params.email;
  try {
    if (!email) throw new Error('email is null');
    const footer = await meetingRepository.getFooter(email);
    res.json({ footer: footer });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({
      devMessage: error.message
    })
  }
}

module.exports.updateMeeting = async (incomingMeetingData) => {
  console.log("==================== updateMeeting ============================");
  // console.log(incomingMeetingData);
  let meetingId = incomingMeetingData.id;
  let isCertSent = meetingRepository.checkIfCertTrigForMeeting(meetingId);
  // let condition = { where: { 'id': meetingId } };
  // let meetingData = await meetingRepository.meetingData(condition);
  let meetingData = await checkMeeting(incomingMeetingData);
  console.log(meetingData);
  try {
    if (meetingData.message){
      let meetingInvites = meetingData.invitees;
      console.log("meetingInvites");
      console.log(meetingInvites);
      
      const stateMachineArn = meetingData.arnName;
      

      if(meetingInvites.length == 0){
          return this.cancelMeeting(meetingId);
      }
        
      if (stateMachineArn) {
          await this.stopStepFunction(stateMachineArn);
      }
      
      let userDetail = await userRepository.organizationSettingsById(meetingData.user_id);
      // console.log(userDetail);
      let mailTimes;
      if (userDetail[0]) {
        let addHourinToSecond = userDetail[0].hour * 60 * 60;
        let addMinuteinToSecond = userDetail[0].minute * 60;

        if (userDetail[0].emailSend == 'After') {
          let totalSeconds = addHourinToSecond + addMinuteinToSecond;
          mailTimes = findSeconds(new Date().toISOString(), meetingData.end_date);
          mailTimes = mailTimes + totalSeconds;
        } else {
          let totalSeconds = addHourinToSecond + addMinuteinToSecond;
          mailTimes = findSeconds(new Date().toISOString(), meetingData.start_date);
          mailTimes = mailTimes - totalSeconds;
        }
        const firstDate = new Date().setHours(12, 0, 0, 0);
        const secondDate = new Date(userDetail[0].trialEnd);
        let user_status = userDetail[0].user_status;
        if (secondDate > firstDate) {
          user_status = 'active'
        }
        console.log("status", user_status);
        userDetail[0].startTime = meetingData.start_date;
        userDetail[0].TimeZoneOffset = meetingData.TimeZoneOffset;

        //opt-out check meetings starts here
        let optOutVal = [], countable = 1;
        if (meetingInvites) {
          optOutVal = meetingInvites.filter(function (listItem) {
            return listItem.email == 'opt-out@sustainably.run';
          });
        }
        if (optOutVal.length > 0) {
          countable = 0;
        }

        // const twodays_inseconds = 60 * 60;
        const twodays_inseconds = 60 * 60 * 48;
        let total_seconds = findSeconds(new Date().toISOString(), meetingData.start_date);
        if (total_seconds < twodays_inseconds) {
          if (user_status != 'inactive' && userDetail[0].status != 'force_inactive' && countable == 1) {
            let stepfunc = await createStepFunction(meetingInvites, mailTimes, userDetail[0], meetingData.id)
            console.log("stepfunctionnnnnn", stepfunc)
            let newstateMachineArnUpdate = await meetingRepository.updateMeeting({ 
              arnName: stepfunc.executionArn, 
              start_date: meetingData.start_date, 
              end_date : meetingData.end_date 
            }, meetingData.id)
            console.log(`Update meeting response ${newstateMachineArnUpdate}`);
          }
        }

        let removeInvitees = await meetingRepository.removeInvitees(meetingData.id);
        console.log(`delete  from meeting_invites ${meetingData.id}`);
        console.log(removeInvitees);
        let inviters = createInvites(meetingInvites, meetingData.id)

        console.log(inviters)
        let insetInvitees = await meetingRepository.insertInvitees(inviters);
        // console.log(insetInvitees)
        console.log("The Invites has been Updated.")
      }
    }
    if(isCertSent) {
      //Scenario : Cert is already sent. Meeting is yet to take place. 
      //Organiser changed the meeting time. For those cert already sent should not be sent again
      //For new invitees if added , cert should trigger for them.
      console.log("Not Creating new step function for already sent invites . Updating meeting details")
    }
  } catch (error) {
    console.log(error);
  }
  return true;
};

function createInvites(obj, meeting_id) {
  let invites = []
  for (let i = 0; i < obj.length; i++) {
    let inviteIndiividual = {};
    inviteIndiividual["first_name"] = obj[i].firstname;
    inviteIndiividual["last_name"]  = obj[i].lastname;
    inviteIndiividual["meeting_id"] =meeting_id;
    inviteIndiividual["email"]  = obj[i].email;
    inviteIndiividual["title"]  = obj[i].title || " ";
    inviteIndiividual["company"]  = obj[i].company;
    inviteIndiividual["createdAt"]  = new Date();
    inviteIndiividual[" updatedAt"] = new Date();
    invites.push(inviteIndiividual);
  }
  return invites;
}
async function createStepFunction(invites, delay, setting, id) {
  console.log('delayyyyyyyyyyyyyyyyyyyyyyyyyyyyyydelay', delay)
  delay = (Math.round(delay) < 0) ? 1 : Math.round(delay);
  try {
    // const stateMachineArn = "arn:aws:states:eu-west-1:255593839762:stateMachine:emailScheduler_sustainable";
    // const stateMachineArn = "arn:aws:states:eu-west-1:574877062696:stateMachine:emailScheduler_sustainable_dev";
    console.log("process.env.AWS_ARN");
    console.log(process.env.AWS_ARN);
    const stateMachineArn = process.env.AWS_ARN;
    const newstateMachineArn = await stepfunctions.startExecution({
      stateMachineArn,
      input: JSON.stringify({
          "delay_seconds": Math.round(delay),
          "obj": invites,
          "id": id,
          "setting": setting
        }),
    }).promise();
  return newstateMachineArn;
  } catch (error) {
    throw error;
  }
}
module.exports.stopStepFunction = async (executableStateMachineArn) => {
  try {
    var params = {
      executionArn: executableStateMachineArn /* required */
    };
    let stopStepfunctionresult = await stepfunctions.stopExecution(params).promise();
    console.log(`State machine ${executableStateMachineArn} executed successfully`, stopStepfunctionresult);
    return stopStepfunctionresult;
  } catch (error) {
      // throw error
      console.log(error);
  }
}
function findSeconds(from, to) {
  var t1 = new Date(to);
  var t2 = new Date(from);
  var dif = t1.getTime() - t2.getTime();
  var Seconds_from_T1_to_T2 = dif / 1000;
  // var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
  console.log(`to : ${to}, from : ${from} --- to : ${t1}, from : ${t2}`)
  // return Seconds_Between_Dates;
  return Seconds_from_T1_to_T2;
}

module.exports.cancelMeeting = async (meetingId) => {
  let meetingDataChk = await meetingRepository.getCancelMeetingDetailsWithNullCheck(meetingId)
  console.log(meetingDataChk);
  if (!meetingDataChk.length) {
    let condition = { where: { 'meeting_id': meetingId } };
    let meetingData = await meetingRepository.meetingData(condition);
    let removeData = await meetingRepository.removeMeeting(condition);
    console.log(meetingData);
    if (meetingData) {
      let removeInvitees = await meetingRepository.removeInvitees(meetingData.id);
      
      const stateMachineArn = meetingData.arnName;
      // const stateMachineArn = "arn:aws:states:eu-west-1:574877062696:execution:emailScheduler_sustainable_dev:c1c81ed8-c20d-41f5-b90c-2b5d238e42c4"
      if (stateMachineArn) {
        await this.stopStepFunction(stateMachineArn);
      }
    }
  }
  return true;
}

module.exports.createMeeting = async (calendarResource, userId) => {
  console.log("createMeeting function :", userId);
  try {
    let getMeeting = await meetingRepository.getMeetingDetails(calendarResource.iCalUId);
    if (!getMeeting.length) {
      console.log(`No Meeting Found With id ${calendarResource.iCalUId}`);
      let incomingInviteeDetailed = [];
      let meetingInvites = [];
      for (let j = 0; j < calendarResource.attendees.length; j++) {
        if ((calendarResource.attendees[j].status.response != "declined") && (calendarResource.attendees[j].emailAddress.address != calendarResource.organizer.emailAddress.address)) {
          meetingInvites.push(calendarResource.attendees[j].emailAddress.address)
          incomingInviteeDetailed.push({ firstname: calendarResource.attendees[j].emailAddress.name, email: calendarResource.attendees[j].emailAddress.address });
        }
      }

      //invitessssss - 
      console.log(incomingInviteeDetailed);
      console.log("calendarResource", calendarResource);

      const location = calendarResource.location.displayName || null;
      let timeZoneOffset = await timeZoneDetailsRepo.getTimeZoneOffsetValue(calendarResource.originalStartTimeZone);
      let meetingData = {
        outlook_meeting_id: calendarResource.iCalUId,
        meeting_id: calendarResource.id,
        subject: calendarResource.subject,
        location: location,
        type: calendarResource.type,
        start_date: calendarResource.start.dateTime,
        end_date: calendarResource.end.dateTime,
        TimeZoneOffset: timeZoneOffset,
        TimeZoneName: calendarResource.originalStartTimeZone,
        cancellation_date: null,
        user_id: userId,
        initiativeId: 182,
        countable: 1,
        rm_id : calendarResource['rm_id'] !== undefined ? calendarResource['rm_id'] : null,
        all_Day : calendarResource.isAllDay ? 'Y' : 'N' //adding a new all day field to be added in table
      }


      //opt-out check meetings starts here
      let optOutVal = [];
      if (incomingInviteeDetailed) {
          optOutVal = incomingInviteeDetailed.filter(function (listItem) {
              return listItem.email == 'opt-out@sustainably.run';
          });
      }
      
      if(calendarResource['bodyPreview'].includes("<---")) {
        meetingData.countable = 1;
      }
      if(optOutVal.length>0) {
        meetingData.countable = 0;
      }
      //opt-out check meetings ends here

      

      let userDetail = await userRepository.organizationSettingsById(userId);
      // console.log(userDetail);
      let mailTimes;
      if (userDetail[0]) {
        let addHourinToSecond = userDetail[0].hour * 60 * 60;
        let addMinuteinToSecond = userDetail[0].minute * 60;

        if (userDetail[0].emailSend == 'After') {
          let totalSeconds = addHourinToSecond + addMinuteinToSecond;
          mailTimes = findSeconds(new Date().toISOString(), meetingData.end_date);
          mailTimes = mailTimes + totalSeconds;
        } else {
          let totalSeconds = addHourinToSecond + addMinuteinToSecond;
          mailTimes = findSeconds(new Date().toISOString(), meetingData.start_date);
          mailTimes = mailTimes - totalSeconds;
        }
        const firstDate = new Date().setHours(12, 0, 0, 0);
        const secondDate = new Date(userDetail[0].trialEnd);
        let user_status = userDetail[0].user_status;
        if (secondDate > firstDate) {
          user_status = 'active'
        }
        console.log("status", user_status);
        userDetail[0].startTime = meetingData.start_date;
        userDetail[0].TimeZoneOffset = meetingData.TimeZoneOffset;
        userDetail[0].TimeZoneName = meetingData.TimeZoneName;

        // const twodays_inseconds = 60 * 60;
        const twodays_inseconds = 60 * 60 * 480000000;
        let total_seconds = findSeconds(new Date().toISOString(), meetingData.start_date);
        if (total_seconds < twodays_inseconds) {
          if (user_status != 'inactive' && userDetail[0].status != 'force_inactive' && meetingData.countable == 1) {

            // meetingData.arnName = stepfunc.executionArn;
            meetingData.org_id = userDetail[0].orginasation_id;
            console.log(meetingData)
            let meetingSave = await meetingRepository.createMeeting(meetingData);
            console.log(`Meeting Data inserted meeting id  ${meetingSave.dataValues.id}`);
            if (meetingSave && meetingSave.dataValues) {
              let inviters = createInvites(incomingInviteeDetailed, meetingSave.dataValues.id)
              let stepfunc = await createStepFunction(incomingInviteeDetailed, mailTimes, userDetail[0], meetingSave.dataValues.id)
              console.log("stepfunctionnnnnn", stepfunc)
              let arnName = stepfunc.executionArn;
              let newstateMachineArnUpdate = await meetingRepository.updateMeeting({ arnName: arnName}, meetingSave.dataValues.id)
              console.log(`Update meeting response ${newstateMachineArnUpdate}`);
              // console.log(inviters)
              let insetInvitees = await meetingRepository.insertInvitees(inviters);
              // console.log(insetInvitees)
            }
          }
        }
      }
    } else if (getMeeting[0].web_meeting_id == null)  {
      console.log("Meeting Details Found");
      let updateMeetingId = await meetingRepository.updateMeeting({ meeting_id: calendarResource.id }, getMeeting[0].MeetingId);
      console.log(`Update meetingId ${updateMeetingId}`);
    }
  } catch (error) {
    console.log(error);
  }
  return true;
}

async function checkMeeting(incomingMeetingData) {
  let meetingData = await meetingRepository.getMeetingDetails(incomingMeetingData.iCalUId, true)
  // console.log(meetingData);
  if (meetingData.length && meetingData[0].outlook_meeting_id){
    /*------------------------------------
        Meeting Invitees Checking Start   
    --------------------------------------*/
    let incomingInviteeList=[];
    let incomingInviteeDetailed=[];
    console.log(incomingMeetingData.attendees);
    for (let j = 0; j < incomingMeetingData.attendees.length; j++) {
        if ((incomingMeetingData.attendees[j].status.response != "declined") && (incomingMeetingData.attendees[j].emailAddress.address != incomingMeetingData.organizer.emailAddress.address)){
        incomingInviteeList.push(incomingMeetingData.attendees[j].emailAddress.address)
        incomingInviteeDetailed.push({ firstname : incomingMeetingData.attendees[j].emailAddress.name, email : incomingMeetingData.attendees[j].emailAddress.address})
      }
    }
    let inviteesList = [];
    let inviteeCheck = [];
    for (let i = 0; i < meetingData.length; i++) {
      inviteesList.push(meetingData[i].email)
      if (!incomingInviteeList.includes(meetingData[i].email)){
        inviteeCheck.push(meetingData[i].email);
      }
    }
    // console.log(inviteeCheck);
    // console.log(incomingInviteeList);
    // console.log(inviteesList);
    if (inviteeCheck.length == 0){
      inviteeCheck = incomingInviteeList.filter(function (item) {
        return !inviteesList.includes(item);
      })
    }
    console.log(inviteeCheck);
    let result = { 
      "message": true, 
      "invitees": incomingInviteeDetailed, 
      "arnName": meetingData[0].arnName, 
      "user_id": meetingData[0].user_id,
      "start_date": incomingMeetingData.start.dateTime,
      "end_date": incomingMeetingData.end.dateTime,
      "TimeZoneOffset": meetingData[0].TimeZoneOffset,
      "id": meetingData[0].MeetingId,
    }
    if (inviteeCheck.length != 0) {
      return result
    }
    /*------------------------------------
        Meeting Invitees Checking End
    --------------------------------------*/
  
    /*------------------------------------
      Meeting Details Checking Start
    --------------------------------------*/
    // subject: calendarResource.subject,
    let start_date = new Date(meetingData[0].start_date).getTime();
    let end_date = new Date(meetingData[0].end_date).getTime();
    let incomingStart_date = new Date(incomingMeetingData.start.dateTime).getTime();
    let incomingEnd_date = new Date(incomingMeetingData.end.dateTime).getTime();
    console.log(`start_date,end_date, incomingStart_date, incomingEnd_date`);
    console.log(start_date,end_date, incomingStart_date, incomingEnd_date);
    
    if ((start_date != incomingStart_date) || (end_date != incomingEnd_date)){
      return result
    }
    /*------------------------------------
    Meeting Details Checking End
    --------------------------------------*/
  }
  return {"message" : false}
}

